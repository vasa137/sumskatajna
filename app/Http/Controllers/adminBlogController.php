<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use Redirect;
use File;
class adminBlogController extends Controller
{
    private function obrisi_temp(){
        $directoryPath = public_path('images/clanci/temp');
        File::deleteDirectory($directoryPath);

        File::makeDirectory($directoryPath,0755,true);
    }

	public function blog()
	{
		$blog= Blog::dohvatiSveAktivne();
        
        $obrisani = Blog::dohvatiSveObrisane();

		return view('admin.adminBlog',compact('blog','obrisani'));
	}


	public function clanak($id)
	{
        $this->obrisi_temp();

		$izmena = false;

		if ($id>0)
		{
			$izmena = true;
			$clanak=Blog::dohvatiSaId($id);

            $clanak_directory = public_path('images/clanci/' . $clanak->id);
            $temp_directory = public_path('images/clanci/temp');

            if(!File::exists($clanak_directory)){
                File::makeDirectory($clanak_directory);
            }

            File::copyDirectory($clanak_directory, $temp_directory);

            // ZA ADMINA SLIKU VRACAMO KAO GLAVNA.JPG
            if(File::exists($temp_directory . '/' . $clanak->naslov . '.jpg')) {
                File::move($temp_directory . '/' . $clanak->naslov . '.jpg', $temp_directory . '/glavna.jpg');
            }

			return view('admin.adminClanak',compact('izmena','clanak'));
		}

		return view('admin.adminClanak',compact('izmena'));
	}


	public function sacuvaj($id)
	{
		$izmena = false;

	    if($id > 0){
	        $izmena = true;
        }

	    $naslov = $_POST['naslov'];
	    $tekst = $_POST['tekst'];
	    $uvod = $_POST['uvod'];


        if($izmena){
            $clanak = Blog::dohvatiSaId($id);
        } else{
            $clanak = new Blog();
        }

        $clanak->napuni($naslov, $uvod, $tekst);

        $clanak_directory = public_path('images/clanci/' . $clanak->id);
        $temp_directory = public_path('images/clanci/temp');

        if(File::exists($clanak_directory)){
            File::deleteDirectory($clanak_directory);
        }

        File::makeDirectory($clanak_directory,0755,true);
        File::copyDirectory($temp_directory, $clanak_directory);

        // PROMENA NAZIVA GLAVNE SLIKE ZBOG OPTIMIZACIJE
        if(File::exists($clanak_directory . '/glavna.jpg')) {
            File::move($clanak_directory . '/glavna.jpg', $clanak_directory . '/' . $clanak->naslov . '.jpg');
        }

        $this->obrisi_temp();

	    return redirect('/admin/clanak/' . $clanak->id);
	}

    public function restauriraj($id)
    {
        $clanak= Blog::dohvatiSaId($id);

        $clanak->restauriraj();

        return Redirect::back();
    }

	public function obrisi($id)
	{
		$clanak= Blog::dohvatiSaId($id);

		$clanak->obrisi();

        return Redirect::back();
	}

    public function upload_slike(){
        $image = $_FILES['file'];

        $directoryPath = public_path('images/clanci/temp');
        $image_name = 'glavna.jpg';

        //$filename = time() . '.' . pathinfo('' . $image['name'][0], PATHINFO_EXTENSION);

        File::move($image['tmp_name'][0], $directoryPath . '/' . $image_name);

        chmod($directoryPath . '/' .$image_name, 0644);
    }

    public function obrisi_upload_slike(){
        $image_name = $_POST['filename'];

        $directoryPath = public_path('images/clanci/temp');

        File::delete($directoryPath . '/' . $image_name);
    }
}
