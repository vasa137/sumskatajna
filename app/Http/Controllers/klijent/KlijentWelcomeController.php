<?php

namespace App\Http\Controllers\klijent;

use App\Proizvod;
use App\Blog;
use App\Utility\Util;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Mail;
use Carbon\Carbon;

class KlijentWelcomeController extends Controller
{
    private function popuniProizvodeNazivimaGlavnihSlika($proizvodi){
        foreach($proizvodi as $proizvod){
            $proizvod->nazivGlavneSlike = Util::getInstance()->nazivGlavneSlike($proizvod);
        }
    }

    public function welcome(){
        $proizvodi = Proizvod::dohvatiSveAktivne();
        
        $this->popuniProizvodeNazivimaGlavnihSlika($proizvodi);

        $blog=Blog::dohvatiSveAktivne();

        $blog = $blog->reverse();

        $recepti=Blog::dohvatiSveRecepte()->reverse();

        $recepti = $recepti->reverse();
        
//dd($blog,$recepti);

        return view('welcome', compact('proizvodi','blog','recepti'));
    }


    public function kontaktiraj(Request $request)
    {

        
        $request['captcha'] = $this->captchaCheck();

        $request->validate([
            'ime_prezime' => 'required|string|max:254',
            'telefon' => 'required|string|max:19',
            'mail' => 'required|email|max:254',
            'g-recaptcha-response' => 'required',
            'captcha' => 'required|min:1',
        ]);
    
        $ime_prezime = $_POST['ime_prezime'];
        $telefon = $_POST['telefon'];
        $mail = $_POST['mail'];
        $poruka = $_POST['poruka'];
        $vreme = Carbon::now();

        $vreme->tz='Europe/Belgrade';


         $data =[
           'ime_prezime' => $ime_prezime,
           'telefon' =>  $telefon,
           'poruka' => $poruka,
           'mail' => $mail,
           'vreme' => $vreme

        ];


  //return view('mail.kontakt' ,compact('ime_prezime','telefon','vreme','mail','poruka'));
        //-----------------slanje maila-----------
        
         Mail::send('mail.kontakt', $data, function($message) use ($ime_prezime,$mail) {
         $message->to('info@sumskatajna.rs', 'Poruka sa sajta- '.$ime_prezime)->subject('Poruka sa sajta- '.$ime_prezime);
         $message->from('prodaja@sumskatajna.rs' ,$ime_prezime.' - Poruka sa sajta');
        });
    
        //dd($data);


        //return view('mailovi.kontakt',compact('vreme','mail','poruka','telefon','ime_prezime'));

     return redirect('/kontakt-uspesan');
    }


}
