<?php

namespace App\Http\Controllers\klijent;

use App\Proizvod;
use App\ProizvodTag;
use App\Tag;
use App\ProizvodKategorija;
use App\Kategorija;
use App\Utility\Util;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Route;
class KlijentProizvodiController extends Controller
{
    private function popuniProizvodeNazivimaGlavnihSlika($proizvodi){
        foreach($proizvodi as $proizvod){
            $proizvod->nazivGlavneSlike = Util::getInstance()->nazivGlavneSlike($proizvod);
        }
    }


     public function kategorija($link,$id){
        

        $izabrana_kategorija=Kategorija::dohvatiSaId($id);

        $proizvodi = Proizvod::filtriraj($id);
       

        //dd($brKat,$izabrana_kategorija,$proizvodi);

        $kategorije = Kategorija::dohvatiAktivneKategorijeSortiranePoPrioritetu();

        $this->popuniProizvodeNazivimaGlavnihSlika($proizvodi);

        return view('kategorija', compact('proizvodi','kategorije','izabrana_kategorija'));
    }

    public function prodavnica(){
        $proizvodi = Proizvod::dohvatiSveAktivne();

        $kategorije = Kategorija::dohvatiAktivneKategorijeSortiranePoPrioritetu();
        $this->popuniProizvodeNazivimaGlavnihSlika($proizvodi);

        return view('prodavnica', compact('proizvodi','kategorije'));
    }

    public function proizvod($link, $id){
        $proizvod = Proizvod::dohvatiSaId($id);

        if($proizvod == null || $proizvod->sakriven){
            abort(404);
        }

        if($proizvod->ima_tagove){
            $proizvodNizTagova = [];
            $proizvodTagovi = ProizvodTag::dohvatiTagoveZaProizvod($proizvod->id);

            foreach ($proizvodTagovi as $proizvodTag) {
                $proizvodNizTagova[] = Tag::dohvatiSaId($proizvodTag->id_tag);
            }

            $proizvod->tagovi = $proizvodNizTagova;
        }

        $proizvodDirectory =  public_path('images/proizvodi/' . $proizvod->id);

        $proizvod->nazivGlavneSlike = Util::getInstance()->nazivGlavneSlike($proizvod);

        $proizvod->sveSlike = Util::getInstance()->pokupiNizFajlova($proizvodDirectory . '/sveSlike');

        return view('proizvod', compact('proizvod'));
    }
}
