<?php

namespace App\Http\Controllers\klijent;

use App\Proizvod;
use App\ProizvodTag;
use App\Blog;
use App\Utility\Util;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

use Route;
class KlijentBlogController extends Controller
{
   
    public function blog(){
        $clanci = Blog::dohvatiSveAktivne();
        $clanci = $clanci->reverse();
   // dd($clanci);
        return view('blog', compact('clanci'));
    }

    public function recepti(){
        $clanci = Blog::dohvatiSveRecepte();
        
        $clanci = $clanci->reverse();
        return view('recepti', compact('clanci'));
    }

    public function clanak($id){
        $clanak = Blog::dohvatiSaId($id);

        if($clanak == null || $clanak->sakriven){
            abort(404);
        }

        return view('clanak', compact('clanak'));
    }

    public function recept($id){
        $clanak = Blog::dohvatiSaId($id);

        if($clanak == null || $clanak->sakriven){
            abort(404);
        }

        return view('recept', compact('clanak'));
    }
}
