<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kupon extends Model
{
    protected $table = 'kupon';

    protected $fillable = ['naziv', 'kod', 'popust', 'ogranicen', 'broj_koriscenja', 'max_koriscenja', 'aktivan', 'sakriven', 'za_sve_kategorije', 'jos_proizvoda', 'za_sve_kupce', 'za_snizene'];

    protected $appends = ['kategorije', 'proizvodi', 'korisnici', 'ukupno_ustedjeno'];

    protected $kategorije;
    protected $proizvodi;
    protected $korisnici;
    protected $ukupno_ustedjeno;

    public function setKategorijeAttribute($kategorije){
        $this->kategorije = $kategorije;
    }

    public function getKategorijeAttribute(){
        return $this->kategorije;
    }

    public function setProizvodiAttribute($proizvodi){
        $this->proizvodi = $proizvodi;
    }

    public function getProizvodiAttribute(){
        return $this->proizvodi;
    }

    public function setKorisniciAttribute($korisnici){
        $this->korisnici = $korisnici;
    }

    public function getKorisniciAttribute(){
        return $this->korisnici;
    }

    public function setUkupnoUstedjenoAttribute($ukupno_ustedjeno){
        $this->ukupno_ustedjeno = $ukupno_ustedjeno;
    }

    public function getUkupnoUstedjenoAttribute(){
        return $this->ukupno_ustedjeno;
    }

    public static function dohvatiSaId($id){
        return Kupon::where('id', $id)->first();
    }

    public function napuni($naziv, $kod, $popust, $ogranicen, $broj_koriscenja, $max_koriscenja, $za_sve_kategorije, $jos_proizvoda, $za_sve_kupce, $za_snizene){
        $this->naziv = $naziv;
        $this->kod = $kod;
        $this->popust = $popust;
        $this->ogranicen = $ogranicen;
        $this->broj_koriscenja = $broj_koriscenja;
        $this->max_koriscenja = $max_koriscenja;
        $this->za_sve_kategorije = $za_sve_kategorije;
        $this->jos_proizvoda = $jos_proizvoda;
        $this->za_sve_kupce = $za_sve_kupce;
        $this->za_snizene = $za_snizene;

        $this->save();
    }

    public static function dohvatiSaKodom($kod){
        return Kupon::where('kod', $kod)->first();
    }

    public static function daLiJeJedinstven($kod){
        return self::dohvatiSaKodom($kod) != null ? false : true;
    }

    public function obrisi(){
        $this->aktivan = 0;
        $this->sakriven = 1;

        $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;

        $this->save();
    }

    public function aktiviraj(){
        $this->aktivan = 1;

        $this->save();
    }

    public function deaktiviraj(){
        $this->aktivan = 0;

        $this->save();
    }

    public function daLiJeValidan($kod){
        $kupon = self::dohvatiSaKodom($kod);

        return $kupon->aktivan == 1 && $kupon->sakriven == 0;
    }

    public static function dohvatiSveNeobrisane(){
        return Kupon::where('sakriven', 0)->get();
    }

    public static function dohvatiSveObrisane(){
        return Kupon::where('sakriven', 1)->get();
    }

    public static function dohvatiAktivanKuponSaKodom($kod){
        return Kupon::where('kod', $kod)->where('aktivan', 1)->where('sakriven', 0)->first();
    }
}
