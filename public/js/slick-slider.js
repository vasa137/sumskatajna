$(document).ready(function () {
    var slikkfor = $('.slider-for');
    if(slikkfor.length > 0){
        slikkfor.slick({
            slidesToShow: 1,
            speed: 500,
            arrows: false,
            fade: true,
            asNavFor: '.product-deails-thumb'
        });
    }
    var productDetailsThumb = $('.product-deails-thumb');

    if(productDetailsThumb.length > 0){

        productDetailsThumb.slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            speed: 500,
            asNavFor: '.slider-for',
            dots: false,
            focusOnSelect: true,
            slide: 'li',
            autoplay: true,
            arrows: false
        });

    }



});