function inicijalizujSlider(idProizvoda, sveSlike){
    $(document).ready(function() {
        sveSlike = JSON.parse(sveSlike);
        let nizSlika = [];
        nizSlika.push('/images/proizvodi/' + idProizvoda + '/glavna/Novi-12344.jpg');
        for (let i = 0; i < sveSlike.length; i++) {
            nizSlika.push('/images/proizvodi/' + idProizvoda + '/sveSlike/' + sveSlike[i]);
        }

        console.log(nizSlika);
        $('#slide100-01').slide100({
            autoPlay: "false",
            timeAuto: 3000,
            deLay: 400,

            linkIMG: nizSlika,

            linkThumb: nizSlika.splice(0, 1)
        });
    });
}