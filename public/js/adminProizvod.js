let specifikacijeOriginal;
let grupeOpcijaOriginal;
let opcijePoGrupamaOriginal;

function inicijalizujSpecifikacije(specifikacije){
    specifikacijeOriginal = JSON.parse(specifikacije);
}

function obrisiSpecifikaciju(id){
    let $specifikacijeSelect = $('#specifikacije');
    document.getElementById('spec-' + id).remove();

    let specifikacija = nadjiSpecifikaciju(id);

    $specifikacijeSelect.append('<option id="specifikacija-' + id+ '" value="' + id + '" selected>' + specifikacija.naziv + '</option>')
}

function specifikacijaLayout(specifikacija){
    return '<div id="spec-' +specifikacija.id +'" class="row col-12" >   ' +
    '            <div class="col-10">    ' +
    '                <div class="input-group">\n' +
    '                    <div class="input-group-prepend">\n' +
    '                        <span class="input-group-text">\n' +
    '                            ' + specifikacija.naziv +'\n' +
    '                        </span>\n' +
    '                    </div>\n' +
    '                    <input type="hidden" name="specifikacije[]" value="' + specifikacija.id +'"  />'+
    '                    <input type="text" class="form-control" id="specifikacijaTekst-'+  specifikacija.id +'" name="specifikacijeTekst[]" placeholder="Tekst specifikacije.." required>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col-2">\n' +
    '                <button type="button" onclick="obrisiSpecifikaciju(' +specifikacija.id +')" class="btn btn-danger  mr-5 mb-5">\n' +
    '                    <i class="fa fa-times mr-5"></i>Obriši\n' +
    '                </button>\n' +
    '            </div>' +
    '        </div>';

}



function grupeOpcijaLayout(grupaOpcija){
    return '<div id="grupaOpcija-' +grupaOpcija.id +'" class="row col-12" >   ' +
    '               <div class="col-10">\n' +
        '                <div class="input-group">\n' +
        '                    <div class="input-group-prepend">\n' +
        '                        <span class="input-group-text">\n' +
        '                            Grupa \n' +
        '                        </span>\n' +
        '                    </div>\n' +
        '                    <input type="text" id="grupa-opcija-' + grupaOpcija.id+'" class="form-control" name="grupeOpcija[]" value="'+ grupaOpcija.naziv + '" readonly>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '            <div class="col-2">\n' +
        '                <button onclick="obrisiGrupuOpcija(' + grupaOpcija.id + ')" type="button" class="btn btn-danger  mr-5 mb-5">\n' +
        '                    <i class="fa fa-times mr-5"></i>Obriši\n' +
        '                </button>\n' +
        '            </div>' +
        '</div>';

}

function selectOpcijeZaGrupuLayout(grupaOpcija, opcijeZaGrupu){
    let html = '<select size="4" class="js-select2 form-control" id="opcijeZaGrupu-'+ grupaOpcija.id + '-select" style="width: 100%;" data-placeholder="Choose one..">\n';

    for(let i = 0; i < opcijeZaGrupu.length; i++){
        let opcija = opcijeZaGrupu[i];
        html += '<option value="'+ opcija.id +'">' + opcija.naziv + '</option>\n'
    }

    html += '</select>\n';

    return html;
}

function opcijeZaGrupuLayout(grupaOpcija, opcijeZaGrupu){
        let html = '<div id="opcijeZaGrupu-' + grupaOpcija.id + '" class="block block-rounded block-themed">\n' +
        '            <div class="block-header bg-gd-primary">\n' +
        '                <h3 class="block-title">Opcije iz grupe ' + grupaOpcija.naziv + '</h3>\n' +
        '            </div>\n' +
        '\n' +
        '            <div id="opcijeKontejner-' + grupaOpcija.id +'" class="block">\n' +
        '\n' +
        '\n' +
        '            </div>\n' +
        '\n' +
        '\n' +
        '\n' +
        '            <div class="block block-header-default">\n' +
        '                <div class="block-header block-header-default">\n' +
        '\n' +
        '\n' +
        '                    <div class="col-12">\n' +
        '                        <!-- Select2 (.js-select2 class is initialized in Codebase() -> uiHelperSelect2()) -->\n' +
        '                        <!-- For more info and examples you can check out https://github.com/select2/select2 -->\n';

        html += selectOpcijeZaGrupuLayout(grupaOpcija, opcijeZaGrupu);

        html += '                        <br/>\n' +
        '                    </div>\n' +
        '\n' +
        '\n' +
        '                </div>\n' +
        '                <div class="col-4 offset-3 block-header block-header-default">\n' +
        '\n' +
        '                    <button onclick="dodajNovuOpcijuUGrupu('+ grupaOpcija.id + ')" type="button" class="btn btn-success mr-5 mb-5">\n' +
        '                        <i class="fa fa-plus mr-5"></i>Dodaj novu opciju u grupu ' + grupaOpcija.naziv + '\n' +
        '                    </button>\n' +
        '                    <br/><br/>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '\n' +
        '\n' +
        '\n' +
        '        </div>';

        return html;
}

function opcijaLayout(opcija){
        return '<div id="opcija-'+ opcija.id + '">' +
        '          <div class="block-header block-header-default">\n' +
        '                    <h3 class="block-title">' + opcija.naziv + '</h3>\n' +
        '                    <div class="block-options">\n' +
        '                        <button onclick="obrisiOpciju('+ opcija.id +',' + opcija.id_grupa_opcija +')" type="button" class="btn btn-danger ">\n' +
        '                            <i class="fa fa-times mr-5"></i>Obriši opciju\n' +
        '                        </button>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '                <div class="block-content block-content-full">\n' +
        '                <div class="form-group row">\n' +
        '                    <label class="col-6">Naziv</label>\n' +
        '                    <label class="col-6">Šifra proizvoda</label>\n' +
        '                    <div class="col-6">\n' +
        '                        <input type="hidden" class="form-control" name="opcije[]" value="'+ opcija.id +'">\n' +
        '                        <input readonly type="text" class="form-control" value="'+ opcija.naziv +'">\n' +
        '                    </div>\n' +
        '                    <div class="col-6">\n' +
        '                        <input maxlength="254" id="opcija-' + opcija.id +'-sifra" type="text" class="form-control" name="opcija-' + opcija.id +'-sifra" value="0" required>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '                <div class="form-group row">\n' +
        '                    <label class="col-6">Nabavna Cena</label>\n' +
        '                    <label class="col-6">Cena</label>\n' +
        '\n' +
        '                    <div class="col-sm-6">\n' +
        '                        <div class="input-group">\n' +
        '                            <div class="input-group-prepend">\n' +
        '                                <span class="input-group-text">\n' +
        '                                    <i class="fa fa-fw fa-money"></i>\n' +
        '                                </span>\n' +
        '                            </div>\n' +
        '                            <input id="opcija-' + opcija.id + '-nabavna_cena" type="number" class="form-control" name="opcija-' + opcija.id + '-nabavna_cena"  value="0" required>\n' +
        '                            <div class="input-group-append">\n' +
        '                                <span class="input-group-text">rsd</span>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '\n' +
        '                    <div class="col-sm-6">\n' +
        '                        <div class="input-group">\n' +
        '                            <div class="input-group-prepend">\n' +
        '                                <span class="input-group-text">\n' +
        '                                    <i class="fa fa-fw fa-money"></i>\n' +
        '                                </span>\n' +
        '                            </div>\n' +
        '                            <input id="opcija-' + opcija.id + '-cena" type="number" class="form-control"  name="opcija-' + opcija.id + '-cena"  value="0" required>\n' +
        '                            <div class="input-group-append">\n' +
        '                                <span class="input-group-text">rsd</span>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '\n' +
        '\n' +
        '                <div class="form-group row">\n' +
        '                    <label class="col-12">Cena sa popustom</label>\n' +
        '                    <div class="col-sm-6">\n' +
        '                        <div class="input-group custom-control custom-checkbox">\n' +
        '                            <input id="opcija-' + opcija.id + '-na_popustuCB" class="custom-control-input" type="checkbox" name="opcija-' + opcija.id + '-na_popustu" value="1">\n' +
        '                            <label class="custom-control-label" for="opcija-' + opcija.id + '-na_popustuCB">Proizvod je na sniženju?</label>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                    <div class="col-sm-6">\n' +
        '                        <div class="input-group">\n' +
        '                            <div class="input-group-prepend">\n' +
        '                                <span class="input-group-text">\n' +
        '                                    <i class="fa fa-fw fa-percent"></i>\n' +
        '                                </span>\n' +
        '                            </div>\n' +
        '                            <input id="opcija-' + opcija.id + '-cena_popust" type="number" class="form-control" name="opcija-' + opcija.id + '-cena_popust" value="0" required>\n' +
        '                            <div class="input-group-append">\n' +
        '                                <span class="input-group-text">rsd</span>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '                <div class="form-group row">\n' +
        '                    <label class="col-12" >Lager</label>\n' +
        '\n' +
        '                    <div class="col-12">\n' +
        '                        <label class="css-control css-control-primary css-radio">\n' +
        '                            <input id="opcija-' + opcija.id + '-lager-na_stanju" type="radio" class="css-control-input" name="opcija-' + opcija.id + '-lager" value="na_stanju" checked>\n' +
        '                            <span class="css-control-indicator"></span> Dostupno\n' +
        '                        </label>\n' +
        '                        <label class="css-control css-control-secondary css-radio">\n' +
        '                            <input id="opcija-' + opcija.id + '-lager-van_stanja" type="radio" class="css-control-input" name="opcija-' + opcija.id + '-lager" value="van_stanja">\n' +
        '                            <span class="css-control-indicator"></span> Nedostupno\n' +
        '                        </label>\n' +
        '                        <label class="css-control css-control-secondary css-radio">\n' +
        '                            <input id="opcija-' + opcija.id + '-lager-ne_prikazuj" type="radio" class="css-control-input" name="opcija-' + opcija.id + '-lager" value="ne_prikazuj">\n' +
        '                            <span class="css-control-indicator"></span> Ne prikazuj\n' +
        '                        </label>\n' +
        '                        <label class="css-control css-control-secondary css-radio">\n' +
        '                            <input id="opcija-' + opcija.id + '-lager-prati_broj_komada" type="radio" class="css-control-input" name="opcija-' + opcija.id + '-lager" value="prati_broj_komada">\n' +
        '                            <span class="css-control-indicator"></span> Prati na osnovu količine\n' +
        '                        </label>\n' +
        '                    </div>\n' +
        '                    <label class="col-12">Količina na stanju</label>\n' +
        '                    <div class="col-sm-12">\n' +
        '                        <div class="input-group">\n' +
        '                            <div class="input-group-prepend">\n' +
        '                        <span class="input-group-text">\n' +
        '                            <i class="fa fa-fw fa-archive"></i>\n' +
        '                        </span>\n' +
        '                            </div>\n' +
        '                            <input id="opcija-' + opcija.id + '-br_komada" type="number" class="form-control" name="opcija-' + opcija.id + '-br_komada" value="0" required>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                    <div class="col-sm-12">\n<br/>' +
        '                        <div class="input-group custom-control custom-checkbox offset-3">\n' +
        '                            <input onchange="checkBoxDropZone(this,' + opcija.id +')" id="opcija-' + opcija.id + '-imaSlikeCB" class="custom-control-input" type="checkbox" name="opcija-' + opcija.id + '-ima_slike" value="1">\n' +
        '                            <label class="custom-control-label" for="opcija-' + opcija.id + '-imaSlikeCB">Da li želite posebne slike za ovu opciju?</label>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '                </div>' +
        '</div>';
}

function nadjiSpecifikaciju(id){
    for(let i = 0; i < specifikacijeOriginal.length; i++){
        if(specifikacijeOriginal[i].id == id){
            return specifikacijeOriginal[i];
        }
    }

    return null;
}

function nadjiGrupuOpcija(id){
    for(let i = 0; i < grupeOpcijaOriginal.length; i++){
        if(grupeOpcijaOriginal[i].id == id){
            return grupeOpcijaOriginal[i];
        }
    }

    return null;
}

function nadjiOpciju(idOpcije, idGrupe){
    for(let i = 0; i < opcijePoGrupamaOriginal[idGrupe].length; i++){
        if(opcijePoGrupamaOriginal[idGrupe][i].id == idOpcije){
            return opcijePoGrupamaOriginal[idGrupe][i];
        }
    }

    return null;
}

function dodajSpecifikaciju(id = null){
    let $blokSpecifikacija = $('#blokSpecifikacija');

    if(id == null) {
        let $specifikacijeSelect = $('#specifikacije');
        id = $specifikacijeSelect.val();
    }

    if( id != null){
        $('#specifikacije option[value=\'' + id + '\']').remove();
        $blokSpecifikacija.append(specifikacijaLayout(nadjiSpecifikaciju(id)));
    }


}

function obrisiGrupuOpcija(id){
    let $grupeOpcijaSelect = $('#grupeOpcija');
    document.getElementById('grupaOpcija-' + id).remove();

    document.getElementById('opcijeZaGrupu-' + id).remove();

    for(let i = 0; i <opcijePoGrupamaOriginal[id].length; i++){
        console.log(opcijePoGrupamaOriginal[id][i].id);
        if(document.getElementById('zone-' + opcijePoGrupamaOriginal[id][i].id) != undefined){
            $("#zone-" + opcijePoGrupamaOriginal[id][i].id).css('display', 'none');
        }
    }

    let grupaOpcija = nadjiGrupuOpcija(id);

    $grupeOpcijaSelect.append('<option value="' + id + '" selected>' + grupaOpcija.naziv + '</option>')
}

function obrisiOpciju(idOpcije, idGrupe){
    let $opcijeZaGrupuSelect = $('#opcijeZaGrupu-' + idGrupe + '-select');

    document.getElementById('opcija-' + idOpcije).remove();

    $("#zone-" + idOpcije).css('display', 'none');

    let opcija = nadjiOpciju(idOpcije, idGrupe);

    $opcijeZaGrupuSelect.append('<option value="'+ opcija.id +'" selected>' + opcija.naziv + '</option>\n');
}

function inicijalizujOpcije(grupeOpcija, opcijePoGrupama){
    grupeOpcijaOriginal = JSON.parse(grupeOpcija);
    opcijePoGrupamaOriginal = JSON.parse(opcijePoGrupama);
}

function dodajNovuOpcijuUGrupu(idGrupe, idOpcije = null) {
    let $opcijeKontejner = $('#opcijeKontejner-' + idGrupe);

    if (idOpcije == null) {
        let $opcijeZaGrupuSelect = $('#opcijeZaGrupu-' + idGrupe + '-select');
        idOpcije = $opcijeZaGrupuSelect.val();
    }

    if( idOpcije != null) {
        $('#opcijeZaGrupu-' + idGrupe + '-select option[value=\'' + idOpcije + '\']').remove();

        let opcija = nadjiOpciju(idOpcije, idGrupe);
        let grupa = nadjiGrupuOpcija(idGrupe);

        $opcijeKontejner.append(opcijaLayout(opcija));

        let $desnoKontejner = $('#desnoKontejner');

        // zona ce vec postojati ako je opcija bar jednom bila napravljenja
        if (document.getElementById('zone-' + idOpcije) == undefined) {
            $desnoKontejner.append(dropZoneLayout(opcija, grupa));

            initDropzone(idOpcije, maxFiles);
        }
    }
}

function dodajGrupuOpcija(id = null){
    let $blokGrupaOpcija = $('#blokGrupaOpcija');

    if(id == null) {
        let $grupeOpcijaSelect = $('#grupeOpcija');
        id = $grupeOpcijaSelect.val();
    }

    if( id != null){
        $('#grupeOpcija option[value=\'' + id + '\']').remove();
        let grupaOpcija = nadjiGrupuOpcija(id);
        $blokGrupaOpcija.append(grupeOpcijaLayout(grupaOpcija));

        let $levaStranaKontejner = $('#levaStranaKontejner');

        let opcijeZaGrupu = opcijePoGrupamaOriginal[id];

        $levaStranaKontejner.append(opcijeZaGrupuLayout(grupaOpcija, opcijeZaGrupu));
    }


}

function slikaZonaLayout(idOpcije, slika){
    return '               <div id="slike-opcija-' + idOpcije +'-'+ slika +'" class="col-sm-6 col-xl-4">\n' +
        '                        <div class="options-container">\n' +
        '                            <img class="img-fluid options-item" src="/images/proizvodi/temp/' + idOpcije + '/' +  slika + '" alt="">\n' +
        '                            <div class="options-overlay bg-black-op-75">\n' +
        '                                <div class="options-overlay-content">\n' +
        '                                    <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" href="javascript:obrisiSliku(\'' + slika + '\', ' + idOpcije + ');">\n' +
        '                                        <i class="fa fa-times"></i> Obriši\n' +
        '                                    </a>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>\n';
}

function dropZoneLayout(opcija, grupaOpcija){
    return ' <div id="zone-'+ opcija.id +'" style="display:none" class="block block-rounded block-themed">\n' +
        '            <div class="block-header bg-gd-primary">\n' +
        '                <h3 class="block-title">Slike - opcija ' + opcija.naziv + '  (grupa: ' + grupaOpcija.naziv + ')' +'</h3>\n' +
        '            </div>\n' +
        '            <div  class="block-content block-content-full">\n' +
        '                <!-- Existing Images -->\n' +
        '                <div id="slike-opcija-' + opcija.id + '" class="row gutters-tiny items-push">\n' +
        '\n              </div> ' +
        '                <div class="dropzone" id="dropzone-' + opcija.id +'" name="mainFileUploader">\n' +
        '                    <input type="hidden" id="opcija-dropzone-' + opcija.id + '" name="opcija-dropzone-' + opcija.id +'" value="' + opcija.id+'"/>\n' +
        '                    <div class="fallback">\n' +
        '                        <input name="file" type="file" multiple />\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '\n'             +
        '             </div>' +
        '            </div>\n' +
        '        </div>'
}

function checkBoxDropZone(checkBox, id){
    if(checkBox.checked){
        $('#zone-' + id).css('display', 'block');
    } else{
        $('#zone-' + id).css('display', 'none');
    }
}

// dodaj inpute za kategorije iz treeview-a
function proizvodFormaSubmit(){
    let selectedNodes = $selectableTree.treeview('getSelected');

    $forma = $('#proizvodForma');

    for(let i = 0; i < selectedNodes.length; i++){
        $forma.append('<input type="hidden" name="kategorije[]" value="' +  selectedNodes[i].tags +'" />\n');
    }

    return true;
}

function postaviSpecifikacije(proizvod){
    if(proizvod.specifikacije != null) {
        for (let i = 0; i < proizvod.specifikacije.length; i++) {
            let idSpec = proizvod.specifikacije[i];
            let specifikacijaTekst = proizvod.specifikacije_tekst[i];

            dodajSpecifikaciju(idSpec);
            $('#specifikacijaTekst-' + idSpec).val(specifikacijaTekst);
        }
    }
}

function postaviOpcije(proizvod){
    if(proizvod.grupe_opcija != null) {
        for (let i = 0; i < proizvod.grupe_opcija.length; i++) {
            let idGrupe = proizvod.grupe_opcija[i];

            dodajGrupuOpcija(idGrupe);
            $('#grupa-opcija-' + idGrupe).val(nadjiGrupuOpcija(idGrupe).naziv);

            for (let j = 0; j < proizvod.opcije.length; j++) {

                if (proizvod.opcije[j].grupa_opcija == idGrupe) {
                    let opcija = proizvod.opcije[j];
                    let idOpcije = opcija.id_opcija;

                    dodajNovuOpcijuUGrupu(idGrupe, idOpcije);

                    $('#opcija-' + idOpcije + '-sifra').val(opcija.sifra);
                    $('#opcija-' + idOpcije + '-cena').val(opcija.cena);

                    if (opcija.na_popustu == 1) {
                        $('#opcija-' + idOpcije + '-na_popustuCB').prop('checked', true);
                    }

                    $('#opcija-' + idOpcije + '-cena_popust').val(opcija.cena_popust);
                    console.log(('#opcija-' + idOpcije + '-lager-' + opcija.lager));
                    $('#opcija-' + idOpcije + '-lager-' + opcija.lager).prop('checked', true);
                    $('#opcija-' + idOpcije + '-br_komada').val(opcija.br_komada);
                    $('#opcija-' + idOpcije + '-nabavna_cena').val(opcija.nabavna_cena);

                    if (opcija.ima_slike == 1) {
                        $('#opcija-' + idOpcije + '-imaSlikeCB').prop('checked', true);
                        checkBoxDropZone(document.getElementById('opcija-' + idOpcije + '-imaSlikeCB'), idOpcije);

                        for (let z = 0; z < proizvod.opcijeSlike[idOpcije].length; z++) {
                            $('#slike-opcija-' + idOpcije).append(slikaZonaLayout(idOpcije, proizvod.opcijeSlike[idOpcije][z]));
                        }
                    }
                }
            }
        }
    }
}

function postaviTagove(proizvod){
    if(proizvod.tagovi != null) {
        $('#tagovi').val(proizvod.tagovi).change();
    }
}

function postaviPolja(proizvod){
    proizvod = JSON.parse(proizvod);

    postaviSpecifikacije(proizvod);
    postaviOpcije(proizvod);
    postaviTagove(proizvod);
}

function obrisiSliku(nazivSlike, idOpcije){
    $.ajax({
        url: "/admin/obrisiUploadSlike",
        type: "POST",
        data: {
            "filename": nazivSlike,
            "opcija": idOpcije
        }
    });

    // nemoj sa JQuery posto u ID-ju postoji tacka
    document.getElementById('slike-opcija-' + idOpcije + '-' + nazivSlike).remove();
}
