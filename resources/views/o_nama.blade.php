@extends('layout')

@section('title')
Šumska Priča
@stop

@section('sekcije')
<!-- Title page -->
<section class="bg-img1" style="background-image: url(images/pozadina-heder-namazi-sa-pecurkama.jpg);">
	<div class="container">
		<div class="txt-center p-t-160 p-b-165">
			<h2 class="txt-l-101 cl0 txt-center p-b-14 respon1">
				Šumska Priča
			</h2>

			<span class="txt-m-201 cl0 flex-c-m flex-w">
				<a href="/" class="txt-m-201 cl0 hov-cl10 trans-04 m-r-6">
					Naslovna
				</a>

				<span>
					/ Šumska Priča
				</span>
			</span>
		</div>
	</div>
</section>

<!-- Story -->
<section class="sec-story bg0 p-t-150 p-b-20">
	<div class="container">
		<div class="flex-w flex-sb-t">
			<div class="size-w-31 wrap-pic-w how-shadow2 bo-all-15 bocl0 w-full-md">
				<img src="/images/sumska-prica.jpg" alt="IMG">
			</div>

			<div class="size-w-32 p-t-43 w-full-md">
				<h3 class="txt-center txt-l-401 cl15 p-b-44">
					Šumska Priča
				</h3>

				<p class="txt-center txt-m-115 cl6 p-b-25">
					Kompanija Stanišić Bio pod robnom markom Šumska Tajna je svoje poslovanje započela sa namazima od šumskih ( organskih ) pečurki koji spadaju u visoko kvalitetnu premium hranu.
					<br><br>
					Ova linija proizvoda sadrži pet različitih ukusa sa pečurkama a to su Vrganj, Lisičarka, Crna Truba, Smrčak i Crni Tartuf. 
					<br><br>
					Sirovine nabavljamo od sertifikovanih kompanija čije se otkupne stanice nalaze po planinskim predelima Srbije. Proizvod je namenjen svima onima koji žele zdravo da se hrane, kao i osobama koje imaju specifične potrebe u ishrani jer je naš namaz<strong> posan, veganski, bezglutenski, visoko proteinski, niskokaloričnan i pogodan za dijabetičare (0% šećera).</strong> 
					<br><br>
					Kvalitet sirovina, ukus i pristupačnost našeg proizvoda su učinili da se za kratko vreme izborimo za svoju poziciju na tržištu, ne samo našem već i na tržištu Evropske Unije, Amerike i Crne Gore. Podržani smo od strane Usaid-a u projektu “Konkurentna Privreda” koji traje 4 godine i za cilju ima jačanje srpske privrede. 
					<br><br>
					Osvojili smo nagrade i priznanja, i postali jedna od najuspešnijih startup kompanija u Republici Srbiji. Svakako najveća nagrada za nas je podrška naših potrošača koja nam je omogućila da se razvijamo, zbog čega smo bili u mogućnosti da u naš asortiman uvedemo nove proizvode koje distribuiramo isključivo kanalom online prodaje.
					<br><br>
					Od Decembra 2019 godine pored namaza sa pečurkama u našoj paleti proizvoda se mogu naći i voćni namazi, humusi, brašna, čajevi kao i mnogi drugi proizvodi organskog porekla i neprskanih sirovina.
					<br><br>
					U duhu Kompanije Stanišić Bio svi naši proizvodi su posni, veganski i premium kvaliteta.

				</p>

				

			</div>
		</div>
	</div>
</section>

<!-- Our farmers -->
	<section class="sec-farmer bg0 p-b-70">
		<div class="container">
			<div class="size-a-1 flex-col-c-m p-b-55">
		        <img src="/images/icons/stanisic-bio-sumska-tajna.png" alt="IMG">
		        <div class="txt-center txt-m-201 cl10 how-pos1-parent m-b-14">
		            Ko smo mi?
		        </div>

		        <h3 class="txt-center txt-l-101 cl3 respon1">
		            Naš tim
		        </h3>
		    </div>



			<div class="row">
				<div class="col-sm-12 col-md-3 p-b-30 m-rl-auto">
					<div class="hov10 trans-04">
						<a href="#" class="hov-img0">
							<img src="images/nikola.jpg" alt="IMG">
						</a>

						<div class="flex-col-c-m bg0 p-rl-15 p-t-37 p-b-35">
							<a href="#" class="txt-m-114 cl3 txt-center hov-cl10 trans-04 p-b-9">
								Nikola Stanišić
							</a>

							<span class="txt-s-101 cl6 txt-center">
								Founder & CEO
							</span>

						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-3 p-b-30 m-rl-auto">
					<div class="hov10 trans-04">
						<a href="#" class="hov-img0">
							<img src="images/tatjana.jpg" alt="IMG">
						</a>

						<div class="flex-col-c-m bg0 p-rl-15 p-t-37 p-b-35">
							<a href="#" class="txt-m-114 cl3 txt-center hov-cl10 trans-04 p-b-9">
								Tatjana Jović
							</a>

							<span class="txt-s-101 cl6 txt-center">
								Marketing
							</span>

						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-3 p-b-30 m-rl-auto">
					<div class="hov10 trans-04">
						<a href="#" class="hov-img0">
							<img src="images/ana.jpg" alt="IMG">
						</a>

						<div class="flex-col-c-m bg0 p-rl-15 p-t-37 p-b-35">
							<a href="#" class="txt-m-114 cl3 txt-center hov-cl10 trans-04 p-b-9">
								Ana Jović
							</a>

							<span class="txt-s-101 cl6 txt-center">
								Nutricionista
							</span>

						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-3 p-b-30 m-rl-auto">
					<div class="hov10 trans-04">
						<a href="#" class="hov-img0">
							<img src="images/andrija.jpg" alt="IMG">
						</a>

						<div class="flex-col-c-m bg0 p-rl-15 p-t-37 p-b-35">
							<a href="#" class="txt-m-114 cl3 txt-center hov-cl10 trans-04 p-b-9">
								Andrija Smiljanić
							</a>

							<span class="txt-s-101 cl6 txt-center">
								Co-Owner & IT
							</span>

						</div>
					</div>
				</div>

				
				
			</div>
		</div>
	</section>

	
@stop