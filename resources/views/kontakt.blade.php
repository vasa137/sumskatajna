@extends('layout')

@section('title')
Kontakt 
@stop

@section('scriptsBottom')
  <script src='https://www.google.com/recaptcha/api.js' async defer></script>
@endsection

@section('sekcije')
<!-- Title page -->
<section class="bg-img1" style="background-image: url(images/pozadina-heder-namazi-sa-pecurkama.jpg);">
	<div class="container">
		<div class="txt-center p-t-160 p-b-165">
			<h2 class="txt-l-101 cl0 txt-center p-b-14 respon1">
				Kontakt
			</h2>

			<span class="txt-m-201 cl0 flex-c-m flex-w">
				<a href="/" class="txt-m-201 cl0 hov-cl10 trans-04 m-r-6">
					Naslovna
				</a>

				<span>
					/ Kontakt
				</span>
			</span>
		</div>
	</div>
</section>

<!-- Story -->
<section class="sec-story bg0 p-t-150 p-b-100">
	<div class="container">
			<div class="size-a-1 flex-col-c-m p-b-70">
				<img src="/images/icons/stanisic-bio-sumska-tajna.png" alt="IMG">
				<div class="txt-center txt-m-201 cl10 how-pos1-parent m-b-14">
					KONTAKTK

			
				</div>

				<h3 class="txt-center txt-l-101 cl3 respon1">
					Kontaktirajte nas za sve potrebne informacije.
				</h3>
			</div>
			<form class="validate-form" action="/kontaktiraj" id="kontakt_forma" method="POST">
						{{csrf_field()}}
			
				<div class="row">
					<div class="col-sm-4 p-b-30">
						<div class="validate-input"  data-validate = "Ime je obavezno polje">
							<input required class="txt-s-101 cl3 plh1 size-a-46 bo-all-1 bocl15 focus1 p-rl-20" type="text" name="ime_prezime" placeholder="Ime i prezime *">
						</div>
					</div>

					<div class="col-sm-4 p-b-30">
						<div required class="validate-input" data-validate = "E-mail je obavezno polje">
							<input class="txt-s-101 cl3 plh1 size-a-46 bo-all-1 bocl15 focus1 p-rl-20" type="text" name="mail" placeholder="Email *">
						</div>
					</div>

					<div class="col-sm-4 p-b-30">
						<div>
							<input class="txt-s-101 cl3 plh1 size-a-46 bo-all-1 bocl15 focus1 p-rl-20" type="text" name="telefon" placeholder="Telefon">
						</div>
					</div>


					<div class="col-12 p-b-30">
						<div required class="validate-input" data-validate = "Poruka je obavezno polje">
							<textarea class="txt-s-101 cl3 plh1 size-a-47 bo-all-1 bocl15 focus1 p-rl-20 p-tb-10" name="poruka" placeholder="Poruka"></textarea>
						</div>	
					</div>
				</div>

				<div class="flex-c p-t-10">
					<button type="submit" form="kontakt_forma" value="Submit" class="g-recaptcha flex-c-m txt-s-103 cl0 bg10 size-a-2 hov-btn2 trans-04"   data-sitekey="6LcOPtgUAAAAAL-rzucpsu0dZVyi0MZSkbGpuG43" data-callback='onSubmit'>
						Pošalji
					</button>
				</div>
			</form>
		</div>
</section>
<!-- Ccontact -->
<section class="container p-t-90 p-b-45">
	<div class="row">
		<div class="col-sm-6 col-lg-4 p-b-50">
			<div class="flex-col-c-m p-rl-25">
				<div class="wrap-pic-max-s p-b-25">
					<img src="images/icons/icon-address.png" alt="IMG">
				</div>

				<h5 class="txt-m-114 cl3 txt-center p-b-9">
					Adresa
				</h5>

				<span class="txt-s-101 cl6 txt-center">
					Dunavska 70, 21203 Vetrenik
				</span>
			</div>
		</div>

		<div class="col-sm-6 col-lg-4 p-b-50">
			<div class="flex-col-c-m p-rl-25">
				<div class="wrap-pic-max-s p-b-25">
					<img src="images/icons/icon-phone-03.png" alt="IMG">
				</div>

				<h5 class="txt-m-114 cl3 txt-center p-b-9">
					Telefon
				</h5>

				<span class="txt-s-101 cl6 txt-center">
					+381 61 600 4919 
				</span>
			</div>
		</div>

		<div class="col-sm-6 col-lg-4 p-b-50">
			<div class="flex-col-c-m p-rl-25">
				<div class="wrap-pic-max-s p-b-25 p-t-5">
					<img src="images/icons/icon-mail-03.png" alt="IMG">
				</div>

				<h5 class="txt-m-114 cl3 txt-center p-b-9">
					Email
				</h5>

				<span class="txt-s-101 cl6 txt-center">
					info@sumskatajna.rs
				</span>
			</div>
		</div>
	</div>
</section>
@stop