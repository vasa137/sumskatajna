@extends('layout')

@section('title')
Recepti 
@stop

@section('sekcije')
<!-- Title page -->
<section class="bg-img1" style="background-image: url(images/pozadina-heder-namazi-sa-pecurkama.jpg);">
	<div class="container">
		<div class="txt-center p-t-160 p-b-165">
			<h2 class="txt-l-101 cl0 txt-center p-b-14 respon1">
				RECEPTI
			</h2>

			<span class="txt-m-201 cl0 flex-c-m flex-w">
				<a href="/" class="txt-m-201 cl0 hov-cl10 trans-04 m-r-6">
					Naslovna
				</a>

				<span>
					/ Recepti
				</span>
			</span>
		</div>
	</div>
</section>

<section class="bg0 p-t-100 p-b-95">
	<div class="container">
		<div class="row">
			@foreach($clanci as $c)
			<div class="col-sm-6 p-b-55">
				<div>
					<a href="/recept/{{$c->id}}" class="wrap-pic-w hov4 how-pos5-parent">
						<img style="width: 100%; max-height: 310px;" src="http://sumskatajna.rs/images/clanci/{{$c->id}}/{{$c->naslov}}.jpg" alt="BLOG">
					</a>
					
					<div class="p-t-29">
						<h4 class="p-b-12">
							<a href="/recept/{{$c->id}}" class="txt-m-120 cl3 hov-cl10 trans-04">
								{{$c->naslov}}
							</a>
						</h4>

						<p class="txt-s-101 cl6 p-b-18">
							{{$c->uvod}}
						</p>

						<div class="how-line2 p-l-40">
							<a href="/recept/{{$c->id}}" class="txt-s-105 cl9 hov-cl10 trans-04">
								Detaljnije
							</a>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>

			
@stop