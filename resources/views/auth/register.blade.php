@extends('layouts.app')

@section('scriptsTop')
    <link href="{{asset('css/social-dugmici.css')}}" rel="stylesheet"/>
@endsection

@section('scriptsBottom')
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registracija</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Ime i prezime</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('ime_prezime') ? ' is-invalid' : '' }}" name="ime_prezime" value="{{ old('ime_prezime') }}" required autofocus>

                                @if ($errors->has('ime_prezime'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ime_prezime') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Lozinka</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Potvrda lozinke</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <!-- DA BI funkcija env radila mora se pokrenuti artisan config:clear -->
                        <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}" id="captcha">
                            <div class="g-recaptcha" data-theme="dark" data-sitekey="{{config('app.re_cap_site')}}" style="overflow:hidden;"></div>
                            @if ($errors->has('g-recaptcha-response'))
                                <br/>
                                <span class="invalid-feedback" role="alert" style="display:block;">
                                    <strong>Morate potvrditi da niste robot.</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                   Registruj se
                                </button>
                            </div>
                        </div>


                    </form>
                    <br/>
                    <form action="/redirect/facebook" method="GET">
                         <button type="submit" class="loginBtn loginBtn--facebook col-4">
                             Sign up with Facebook
                         </button>
                    </form>

                    <br/>
                    <form action="/redirect/google" method="GET">
                        <button class="loginBtn loginBtn--google col-4">
                            Sign up with Google
                        </button>
                    </form>
                    <br/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
