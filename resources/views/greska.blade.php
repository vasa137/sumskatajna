@extends('layout')

@section('scriptsTop')
	<!--Owl carosul-->
	<link href="{{asset('css/slick-slider-addons.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.default.css')}}">
	<link  rel="stylesheet"  href="{{asset('assets/js/plugins/slick/slick-theme.css')}}"/>
	<link href="{{asset('css/slick-lightbox.css')}}" rel="stylesheet" />

@endsection

@section('sekcije')

<section class="how-overlay2 bg-img1" style="background-image: url(/images/pozadina-heder-namazi-sa-pecurkama-crni.jpg);">

	<div class="container">
		<div class="txt-center p-t-160 p-b-165">
			<h2 class="txt-l-101 cl0 txt-center p-b-14 respon1">
				Željena stranica ne postoji!
			</h2>

			<span class="txt-m-201 cl0 flex-c-m flex-w">
				 Link je trenutno nedostupan. Proverite da li ste dobro uneli URL stranice.
			</span>
		</div>
	</div>
</section>

<!-- Product detail -->
<section class="sec-product-detail p-t-50 p-b-70"  style="height: 939px; background-image: url(/images/namazi-sa-pecurkama-pozadina.jpg);">
	<div class="container">
		<div class="row">

			<div class="col-md-12 col-lg-12">
				<div class="txt-center" class="p-l-70 p-t-35 p-l-0-lg">
					
					

					<img width="35%" src="{{asset('/images/greska.png')}}">
					<h4 style="color:white;" class="js-name1 txt-l-104 cl3 p-b-16">
						<a style="color: white;" href="/">
							Povratak na naslovnu stranu.
						</a>
					</h4>
					
					



				</div>
			</div>
		</div>

	</div>
</section>



@stop