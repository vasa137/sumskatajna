@extends('layout')

@section('scriptsTop')
<script src="{{asset('js/klijentNaplati.js')}}"></script>
@endsection

@section('sekcije')
<!-- Title page -->
<section class="bg-img1" style="background-image: url(images/pozadina-heder-namazi-sa-pecurkama.jpg);">
	<div class="container">
		<div class="txt-center p-t-160 p-b-165">
			<h2 class="txt-l-101 cl0 txt-center p-b-14 respon1">
				Korpa
			</h2>

			<span class="txt-m-201 cl0 flex-c-m flex-w">
				<a href="/" class="txt-m-201 cl0 hov-cl10 trans-04 m-r-6">
					Naslovna
				</a>

				<span>
					/ Korpa
				</span>
			</span>
		</div>
	</div>
</section>

<div class="bg0 p-t-95 p-b-50">
	<div class="container">
		<!-- Login -->
		<div>
			<!--
			<div class="txt-s-101 txt-center">
				
				<span class="cl3">
					Returning customer?
				</span>
				
				<span class="cl10 hov12 js-toggle-panel1">
					Click here to login
				</span>

			</div>
				ovde kasnije dodajemo nalog   -->
			<div class="how-bor3 p-rl-15 p-tb-28 m-tb-33 dis-none js-panel1">
				<form class="size-w-60 m-rl-auto">
					<p class="txt-s-120 cl9 txt-center p-b-26">
						If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing & Shipping section.
					</p>

					<div class="row">
						<div class="col-sm-6 p-b-20">
							<div>
								<div class="txt-s-101 cl6 p-b-10">
									Username or email <span class="cl12">*</span>
								</div>

								<input class="txt-s-120 cl3 size-a-21 bo-all-1 bocl15 p-rl-15 focus1" type="text" name="username">
							</div>
						</div>

						<div class="col-sm-6 p-b-20">
							<div>
								<div class="txt-s-101 cl6 p-b-10">
									Password <span class="cl12">*</span>
								</div>

								<input class="txt-s-120 cl3 size-a-21 bo-all-1 bocl15 p-rl-15 focus1" type="password" name="password">
							</div>
						</div>

						<div class="col-12">
							<button class="flex-c-m txt-s-105 cl0 bg10 size-a-21 hov-btn2 trans-04 p-rl-10">
								Login
							</button>

							<div class="flex-w flex-m p-t-10 p-b-3">
								<input id="check-creatacc" class="size-a-35 m-r-10" type="checkbox" name="creatacc">
								<label for="check-creatacc" class="txt-s-101 cl9">
									Create an account?
								</label>
							</div>

							<a href="#" class="txt-s-101 cl9 hov-cl10 trans-04">
								Lost your password?
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
		<form action="/sacuvaj_porudzbinu" class="checkout-form" method="POST" onsubmit="return potvrdiPorudzbinu()">
			{{csrf_field()}}
		<div class="row">

			<div class="col-md-7 col-lg-8 p-b-50">

				<div>
					<h4 class="txt-m-124 cl3 p-b-28">
						Informacije za dostavu
					</h4>

					<div class="row p-b-50">
						<div class="col-sm-6 p-b-23">
							<div>
								<div class="txt-s-101 cl6 p-b-10">
									Ime i prezime <span class="cl12">*</span>
								</div>

								<input class="txt-s-120 cl3 size-a-21 bo-all-1 bocl15 p-rl-20 focus1" type="text" maxlength="254"  name="ime_prezime" @if(Auth::check()) value="{{Auth::user()->ime_prezime}}" @endif required>
							</div>
						</div>

						<div class="col-sm-6 p-b-23">
							<div>
								<div class="txt-s-101 cl6 p-b-10">
									E-mail <span class="cl12">*</span>
								</div>

								<input class="txt-s-120 cl3 size-a-21 bo-all-1 bocl15 p-rl-20 focus1" type="email" maxlength="254"  name="email" @if(Auth::check()) value="{{Auth::user()->email}}" @endif required>
							</div>
						</div>

						<div class="col-sm-6 p-b-23">
							<div>
								<div class="txt-s-101 cl6 p-b-10">
									Telefon <span class="cl12">*</span>
								</div>

								<input class="txt-s-120 cl3 size-a-21 bo-all-1 bocl15 p-rl-20 focus1" type="text" maxlength="19" name="telefon" @if(Auth::check()) value="{{Auth::user()->telefon}}" @endif required>
							</div>
						</div>

						<div class="col-sm-6 p-b-23">
							<div>
								<div class="txt-s-101 cl6 p-b-10">
									Adresa <span class="cl12">*</span>
								</div>

								<input class="txt-s-120 cl3 size-a-21 bo-all-1 bocl15 p-rl-20 focus1" type="text" maxlength="254" name="adresa" @if(Auth::check()) value="{{Auth::user()->adresa}}" @endif required>
							</div>
						</div>

						<div class="col-sm-6 p-b-23">
							<div>
								<div class="txt-s-101 cl6 p-b-10">
									Grad <span class="cl12">*</span>
								</div>

								<input class="txt-s-120 cl3 size-a-21 bo-all-1 bocl15 p-rl-20 focus1" type="text" maxlength="254" name="grad" @if(Auth::check()) value="{{Auth::user()->grad}}" @endif required>
							</div>
						</div>

						<div class="col-sm-6 p-b-23">
							<div>
								<div class="txt-s-101 cl6 p-b-10">
									Poštanski broj <span class="cl12">*</span>
								</div>

								<input class="txt-s-120 cl3 size-a-21 bo-all-1 bocl15 p-rl-20 focus1" type="number" min="10000" max="99999" name="zip" @if(Auth::check()) value="{{Auth::user()->zip}}" @endif required>
							</div>
						</div>

						<!--

						<div class="flex-w flex-m p-rl-15 m-t--10">
							<input id="check-ca" class="size-a-35 m-r-10" type="checkbox" name="ca">
							<label for="check-ca" class="txt-s-101 cl9">
								Create an account?
							</label>
						</div>
					-->
					</div>

					<div>
						<div class="txt-s-101 cl6 p-b-10">
							Napomena (ovde se navode gratis teglice)
						</div>

						<textarea maxlength="999" class="plh2 txt-s-120 cl3 size-a-38 bo-all-1 bocl15 p-rl-20 p-tb-10 focus1" name="napomena" placeholder="Napomena, broj stana, sprat, GRATIS TEGLICE..."></textarea>
					</div>
				</div>
			</div>

			<div class="col-md-5 col-lg-4 p-b-50">
				<div class="how-bor4 p-t-35 p-b-40 p-rl-30 m-t-5">
					<h4 class="txt-m-124 cl3 p-b-11">
						Vaša porudžbina
					</h4>

					<div class="flex-w flex-sb-m txt-m-103 cl6 bo-b-1 bocl15 p-b-21 p-t-18">
						<span>
							Cena
						</span>

						<span>
							{{number_format($total, 0, ',', '.')}} din.
						</span>
					</div>
					
					<!--  -->
					<div class="flex-w flex-sb-m txt-s-101 cl6 bo-b-1 bocl15 p-b-21 p-t-18">
						<span>
							Dostava
						</span>

						<span>
							@if($total > 989)
							besplatna 
							@else
							310 din.
							@endif
						</span>
					</div>

					<div class="flex-w flex-sb-m txt-s-101 cl6 bo-b-1 bocl15 p-b-21 p-t-18">
						<span>
							Plaćanje
						</span>

						<span>
							pouzećem
						</span>
					</div>
					<div class="flex-w flex-sb-m txt-m-103 cl6 bo-b-1 bocl15 p-b-21 p-t-18">
						<span>
							UKUPNO
						</span>

						<span>
							@if($total > 989)
							{{number_format($total + 0,0 , ',', '.')}} din.
							@else
							{{number_format($total + 0,0 , ',', '.')}} din. + (310 din. dostava)
							@endif
						</span>
					</div>
					<div class="flex-w flex-sb-m txt-s-101 cl6 bo-b-1 bocl15 p-b-21 p-t-18">
						<input type="checkbox" checked name="uslovi" id="usloviKoriscenjaCB">
						<label for="uslovi">Slažem se sa uslovima kupovine</label> 

					</div>

					<button class="flex-c-m txt-s-105 cl0 bg10 size-a-21 hov-btn2 trans-04 p-rl-10">
						Potvrdi porudžbinu
					</button>
					<p id="uslovi-error" style="color:red; display:none;">Morate prihvatiti uslove korišćenja.</p>
				</div>
			</div>

		</div>
		</form>
	</div>
</div>

	
@stop