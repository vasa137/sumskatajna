@extends('admin.adminLayout')

@section('title')
    Kategorije
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Admin</a>
    <span class="breadcrumb-item active">Kategorije</span>
@stop

@section('heder-h1')
Kategorije
@stop


@section('heder-h2')
Trenutno <a class="text-primary-light link-effect">{{count($aktivneKategorije)}} aktivnih kategorija</a>.
@stop

@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/bootstrap-treeview.css')}}">
    <script src="{{asset('js/adminKategorije.js')}}"></script>
@endsection


@section('scriptsBottom')
    <!-- Page JS Code -->
    <script src="{{asset('js/bootstrap-treeview.js')}}"></script>
    <script src="{{asset('js/pregledKategorija.js')}}"></script>
    <script src="{{asset('js/tabelaKategorije.js')}}"></script>

    <script>inicijalizujKategorije('{!! addslashes(json_encode($stabloKategorija)) !!}');</script>
@endsection

@section('main')
    <div class="row gutters-tiny">
        <!-- All Products -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-circle-o fa-2x text-info-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{count($aktivneKategorije) + count($obrisaneKategorije)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Ukupno kategorija</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END All Products -->

        <!-- Top Sellers -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:prikaziDostupne()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-star fa-2x text-warning-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{count($aktivneKategorije)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Aktivnih</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Top Sellers -->

        <!-- Out of Stock -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:prikaziNedostupne()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-warning fa-2x text-danger-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-danger" data-toggle="countTo" data-to="{{count($obrisaneKategorije)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Obrisanih</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Out of Stock -->

        <!-- Add Product -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="/admin/kategorija/-1">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-archive fa-2x text-success-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-plus"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Dodaj novu kategoriju</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Add Product -->
    </div>
    <!-- END Overview -->

    <!-- Dynamic Table Full Pagination -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 id="kategorije-title" class="block-title">Kategorije</h3>
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
            <div id="kategorije-aktivne" class="col-sm-12">
                <div id="treeview-selectable"></div>
            </div>

            <table id="tabela-kategorije-obrisane" class="table table-bordered table-striped table-vcenter js-dataTable-full" style="display:none;">
                <thead>
                <tr>
                    <th style="width:40%;">Naziv</th>
                    <th class="d-none d-sm-table-cell text-center" style="width:20%;">Različitih proizvoda</th>
                    <th class="d-none d-sm-table-cell text-center" style="width:20%;">Različitih brendova</th>
                    <th class="text-center" style="width:20%;">Akcija</th>
                </tr>
                </thead>
                <tbody>
                @foreach($obrisaneKategorije as $kategorija)
                    <tr>
                        <td class="font-w600">{{$kategorija->naziv}}</td>
                        <td class="d-none d-sm-table-cell text-center">{{$kategorija->broj_proizvoda}}</td>
                        <td class="d-none d-sm-table-cell text-center" >{{$kategorija->broj_brendova}}</td>

                        <td class="text-center">
                            <a href="/admin/kategorija/{{$kategorija->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Izmeni kategoriju">
                                <i class="fa fa-edit"></i>
                            </a>

                            <form method="POST" action="/admin/restaurirajKategoriju/{{$kategorija->id}}" style="display:inline">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Restauriraj kategoriju">
                                    <i class="fa fa-undo"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full Pagination -->
@stop