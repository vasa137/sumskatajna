@extends('admin.adminLayout')

@section('title')
Kuponi
@stop

@section('breadcrumbs')
<a class="breadcrumb-item" href="/admin">Admin</a>
<span class="breadcrumb-item active">Kuponi</span>
@stop

@section('heder-h1')
Kuponi
@stop


@section('heder-h2')
Trenutno <a class="text-primary-light link-effect">{{$brojAktivnih}} aktivnih kupona</a>.
@stop

@section('scriptsTop')
    <script src="{{asset('/js/adminKuponi.js')}}"></script>
@endsection
@section('scriptsBottom')
    <!-- Page JS Code -->
    <script src="{{asset('/js/tabelaKuponi.js')}}"></script>
@endsection




@section('main')
<div class="row gutters-tiny">
    <!-- All Products -->
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-link-shadow" >
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-tag fa-2x text-info-light"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{count($kuponi) + count($obrisaniKuponi)}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Ukupno kupona</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END All Products -->
    <!-- Top Sellers -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:prikaziDostupne()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-star fa-2x text-warning-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{count($kuponi)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Dostupnih</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Top Sellers -->
    <!-- Out of Stock -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:prikaziNedostupne()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-warning fa-2x text-danger-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-danger" data-toggle="countTo" data-to="{{count($obrisaniKuponi)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Obrisanih</div>
                    </div>
                </div>
            </a>
        </div>
    <!-- Add Product -->
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-link-shadow" href="/admin/kupon/-1">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-archive fa-2x text-success-light"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-success">
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Dodaj novi kupon</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Add Product -->


    
</div>
<!-- END Overview -->

<!-- Dynamic Table Full Pagination -->
<div class="block">
    <div class="block-header block-header-default">
        <h3 id="kuponi-title" class="block-title">Kuponi</h3>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
        <table id="tabela-kuponi" class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
                <tr>
                    <th class="text-center" style="width: 25%;">Naziv</th>
                    <th class="" style="width: 15%;">Kod</th>
                    <th class="d-none d-sm-table-cell text-center" style="width: 10%;">popust</th>
                    <th class="d-none d-sm-table-cell text-center" style="width: 15%;">Iskoriscen puta</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Status</th>
                    <th class="text-center" style="width: 15%;">Akcija</th>
                </tr>
            </thead>
            <tbody>
                @foreach($kuponi as $kupon)
                <tr>
                    <td class="text-center">{{$kupon->naziv}}</td>
                    <td class="font-w600">{{$kupon->kod}}</td>
                    <td class="d-none d-sm-table-cell text-center">{{$kupon->popust*100}}%</td>
                    <td class="d-none d-sm-table-cell text-center">{{$kupon->broj_koriscenja}}</td>
                    <td class="d-none d-sm-table-cell">
                        <span @if($kupon->aktivan) class="text-success" @else class="text-danger" @endif> @if($kupon->aktivan) Aktivan @else Neaktivan @endif</span>
                    </td>
                    
                    <td class="text-center">
                        <a href="/admin/kupon/{{$kupon->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Izmeni kupon">
                            <i class="fa fa-edit"></i>
                        </a>


                        <form method="POST" action="/admin/obrisiKupon/{{$kupon->id}}" style="display: inline">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Obriši kupon">
                                <i class="fa fa-times"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <table id="tabela-kuponi-obrisani" class="table table-bordered table-striped table-vcenter js-dataTable-full" style="display:none">
            <thead>
            <tr>

                <th class="text-center" style="width: 25%;">Naziv</th>
                <th class="" style="width: 15%;">Kod</th>
                <th class="d-none d-sm-table-cell text-center" style="width: 10%;">popust</th>
                <th class="d-none d-sm-table-cell text-center" style="width: 15%;">Iskoriscen puta</th>
                <th class="d-none d-sm-table-cell" style="width: 15%;">Status</th>
                <th class="text-center" style="width: 15%;">Akcija</th>
            </tr>
            </thead>
            <tbody>
            @foreach($obrisaniKuponi as $kupon)
                <tr>
                    <td class="text-center">{{$kupon->naziv}}</td>
                    <td class="font-w600">{{$kupon->kod}}</td>
                    <td class="d-none d-sm-table-cell text-center">{{$kupon->popust*100}}%</td>
                    <td class="d-none d-sm-table-cell text-center">{{$kupon->broj_koriscenja}}</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="text-danger">  Neaktivan </span>
                    </td>
                    <td class="text-center">

                        <a href="/admin/kupon/{{$kupon->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Izmeni kupon">
                            <i class="fa fa-edit"></i>
                        </a>


                        <form method="POST" action="/admin/restaurirajKupon/{{$kupon->id}}" style="display:inline">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Restauriraj kupon">
                                <i class="fa fa-undo"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END Dynamic Table Full Pagination -->
@stop