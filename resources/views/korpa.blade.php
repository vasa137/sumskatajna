@extends('layout')



@section('sekcije')
<!-- Title page -->
<section class="how-overlay2 bg-img1" style="background-image: url(/images/bg-07.jpg);">
	<div class="container">
		<div class="txt-center p-t-160 p-b-165">
			<h2 class="txt-l-101 cl0 txt-center p-b-14 respon1">
				Korpa
			</h2>

			<span class="txt-m-201 cl0 flex-c-m flex-w">
				<a href="/" class="txt-m-201 cl0 hov-cl10 trans-04 m-r-6">
					Naslovna
				</a>

				<span>
					/ Korpa
				</span>
			</span>
		</div>
	</div>
</section>

<div class="row " style=" text-align: center; width: 110%; background-color: black; color: white;">
    <div class="container">
        <p><strong>BESPLATNA DOSTAVA ZA SVE PORDUŽBINE PREKO 1990 DIN.</strong> <br><br></p>
    </div>                
</div>

<!-- content page -->
<div class="bg0 p-tb-100">
	<div class="container" id="korpaPrikaz">
		@if(count($stavke) == 0)
			<h3>Korpa je prazna.</h3>
		@else
			<div class="wrap-table-shopping-cart" id="tabele-div" >
				<table class="table-shopping-cart">
					<tr class="table_head bg12">
						<th class="column-1 p-l-30">Proizvod</th>
						<th class="column-2">Kupon</th>
						<th class="column-3">Cena</th>
						<th class="column-4">Količina</th>
						<th class="column-5">Ukupno</th>
					</tr>
					@foreach($stavke as $stavka)
						<tr id="stavka-proizvod-{{$stavka->rowId}}" class="table_row">
						@include('include.popupDialog', ['poruka' => 'Da li ste sigurni da želite da obrišete proizvod iz korpe?', 'linkUspesno' => 'javascript:obrisiIzKorpe(\''. $stavka->rowId . '\',\'' . $stavka->id .'\')', 'dialogId' => $stavka->rowId])
							<td class="column-1">
								<div class="flex-w flex-m">
									<div class="wrap-pic-w size-w-50 bo-all-1 bocl12 m-r-30">
										@if(File::exists(public_path('/images/proizvodi/' .$stavka->id  . '/glavna/' .$stavka->nazivGlavneSlike . '.jpg')))
											<a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $stavka->name))?>/{{$stavka->id}}">
												<img width="100%" src="{{asset('images/proizvodi/'. $stavka->id .'/glavna/' . $stavka->nazivGlavneSlike . '.jpg')}}" alt="cart image">
											</a>
										@endif
									</div>

									<span>
										<a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $stavka->name))?>/{{$stavka->id}}">
											{{$stavka->name}}
										</a>
									</span>
								</div>
							</td>
							<td class="column-2" id="kupon-stavka-{{$stavka->rowId}}">
								@if($stavka->kupon != null) {{$stavka->kupon->kod}} ({{$stavka->kupon->popust*100}}%) @else / @endif
							</td>
							<td class="column-3">
								{{number_format($stavka->price, 0, ',', '.')}} din.
							</td>
							<td class="column-4">
								<div class="wrap-num-product flex-w flex-m bg12 p-rl-10">
									<div onclick="azurirajKolicinuUKorpi('{!! $stavka->rowId !!}', '{!! $stavka->id!!}', parseInt($('#qttotal-stavka-{{$stavka->rowId}}').val()) - 1)" class="btn-num-product-down flex-c-m fs-29"></div>

									<input id="qttotal-stavka-{{$stavka->rowId}}" class="txt-m-102 cl6 txt-center num-product" type="number" name="num-product1" value="{{$stavka->qty}}">

									<div onclick="azurirajKolicinuUKorpi('{!! $stavka->rowId !!}', '{!! $stavka->id!!}', parseInt($('#qttotal-stavka-{{$stavka->rowId}}').val()) + 1)" class="btn-num-product-up flex-c-m fs-16"></div>
								</div>
							</td>
							<td class="column-5">
								<div class="flex-w flex-sb-m">
									<span id="stavka-total-{{$stavka->rowId}}" >
										{{$stavka->total(0, ',', '.')}} din.
									</span>

									<div class="fs-15 hov-cl10 pointer">
										<span onclick="otvoriDialogSaId('{!! $stavka->rowId!!}')" class="lnr lnr-cross"></span>
									</div>
								</div>
							</td>
						</tr>
					@endforeach

				</table>

				<br/><br/>

				@if(count($stavkeVauceri) > 0)
					@include('include.korpaVauceri', ['stavkeVauceri', $stavkeVauceri])
				@endif
			</div>

			<div class="row p-t-68">
				<div class="flex-w flex-sb-m col-md-6">
				<div class="flex-w flex-m m-r-50 m-tb-10">
					<input id="kod" class="size-a-31 bo-all-1 bocl15 txt-s-123 cl6 plh1 p-rl-20 focus1 m-r-30 m-tb-10" type="text" name="coupon" placeholder="Unesite kod kupona">

					<div onclick="primeniKupon()" class="flex-c-m txt-s-105 cl0 bg10 size-a-32 hov-btn2 trans-04 pointer p-rl-10 m-tb-10">
						Primeni kupon
					</div>
					<p id="kupon-text"></p>
				</div>

				</div>
				<div class="col flex-col-r col-md-6">
					<span class="txt-m-123 cl3 p-b-18">
						IZNOS
					</span>
					
					<div class="flex-w flex-m bo-b-1 bocl15 w-full p-tb-18">
						<span class="size-w-58 txt-m-109 cl3">
							Cena
						</span>

						<span id="total-cena"  class="size-w-59 txt-m-104 cl6">
							{{number_format($total,0 , ',', '.')}} din.
						</span>
					</div>

					<div class="flex-w flex-m bo-b-1 bocl15 w-full p-tb-18">
						<span class="size-w-58 txt-m-109 cl3">
							Dostava
						</span>

						<span class="size-w-59 txt-m-104 cl6" id="dostava-val">
							@if($total > 1990)
							besplatna 
							@else
							310 din.
							@endif
						</span>
					</div>


					<div class="flex-w flex-m bo-b-1 bocl15 w-full p-tb-18">
						<span class="size-w-58 txt-m-109 cl3">
							Ukupno
						</span>

						<span id="total-cena-dostava" class="size-w-59 txt-m-104 cl10">
							
							@if($total > 1990)
							{{number_format($total + 0,0 , ',', '.')}} din.
							@else
							{{number_format($total + 0,0 , ',', '.')}} din. + (310 din. dostava)
							@endif
						</span>
					</div>

					<a href="/naplati" class="flex-c-m txt-s-105 cl0 bg10 size-a-34 hov-btn2 trans-04 p-rl-10 m-t-43">
						Završi kupovinu
					</a>
				</div>
			</div>
		@endif
	</div>
</div>

@stop