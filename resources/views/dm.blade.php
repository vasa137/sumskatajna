@extends('layout')

@section('meta')
<meta property="og:title" content="ŠUMSKA TAJNA" />
<meta property="og:description" content="ŠUMSKA TAJNA - PREMIUM FOOD " />
<meta property="og:image" content="http://sumskatajna.rs/images/og-slika.jpg" />
@stop


@section('sekcije')
<section class="bg-img1" style="background-image: url(images/pozadina-heder-namazi-sa-pecurkama.jpg);">
	<div class="container">
		<div class="txt-center p-t-160 p-b-165">
			<h2 class="txt-l-101 cl0 txt-center p-b-14 respon1">
				DM Objekti
			</h2>

			<span class="txt-m-201 cl0 flex-c-m flex-w">
				<a href="/" class="txt-m-201 cl0 hov-cl10 trans-04 m-r-6">
					Naslovna
				</a>

				<span>
					/ DM Objekti
				</span>
			</span>
		</div>
	</div>
</section>
<div class="row " style=" text-align: center; width: 100%;">
    <div class="container">
        <h3><strong>Naše namaze sa pečurkama možete pronaći u sledećeim DM maloprodajama:</strong> <br><br></h3>
    </div>  

</div>
<div class="bg0 p-t-100 p-b-80">
	<div class="container">

		<div class="wrap-table-shopping-cart rs1-table">
				<table class="table-shopping-cart">
					<tr class="table_head bg12">
						<th class="txt-center">Filijala</th>
						<th class="txt-center">Adresa</th>
						<th class="txt-center">PT broj</th>
						<th class="txt-center">Grad</th>
					</tr>


					 <tr class="table_row"><td class="txt-center">3</td><td class="txt-center">Vojvode Stepe 116</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">18</td><td class="txt-center">Dr.Ivana Ribara 131</td><td class="txt-center">11070</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">19</td><td class="txt-center">Milutina Milankovića 34</td><td class="txt-center">11070</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">30</td><td class="txt-center">Nehruova 68b</td><td class="txt-center">11070</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">45</td><td class="txt-center">Narodnih heroja 63</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">73</td><td class="txt-center">Vareška 4</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">84</td><td class="txt-center">Darinke Radović 33</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">105</td><td class="txt-center">Zaplanjska 32</td><td class="txt-center">11010</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">108</td><td class="txt-center">Radnička 9</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">110</td><td class="txt-center">Kralja Milana 35</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">12</td><td class="txt-center">Bulevar Mihajla Pupina 181</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">24</td><td class="txt-center">Nevesinjska 2</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">29</td><td class="txt-center">Glavna 20</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">33</td><td class="txt-center">Milentija Popovića 32</td><td class="txt-center">11070</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">35</td><td class="txt-center">Brankova 3</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">37</td><td class="txt-center">Marijane Gregoran 58</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">46</td><td class="txt-center">Omladinskih brigada 100</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">48</td><td class="txt-center">Mirijevski bulevar 18b</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">51</td><td class="txt-center">Vojvode Stepe Stepanovića 9I</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">52</td><td class="txt-center">Ustanička 64a</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">54</td><td class="txt-center">Bulevar Arsenija Čarnojevica 95a</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">56</td><td class="txt-center">Batajnicki drum 1</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">60</td><td class="txt-center">Terazije 15-23</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">69</td><td class="txt-center">Bulevar umetnosti 4</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">72</td><td class="txt-center">Vidikovacki venac 102a</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">75</td><td class="txt-center">Vojvode Stepe 344</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">76</td><td class="txt-center">Bulevar Mihajla Pupina 4</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">78</td><td class="txt-center">Beogradskog bataljona 4</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">79</td><td class="txt-center">Živka Davidovića 86</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">80</td><td class="txt-center">Bulevar Kralja Aleksandra 249</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">87</td><td class="txt-center">Patrijarha Dimitrija 14</td><td class="txt-center">11090</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">92</td><td class="txt-center">Tošin bunar 172</td><td class="txt-center">11070</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">93</td><td class="txt-center">Bratstva i jedinstva 2g</td><td class="txt-center">11211</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">101</td><td class="txt-center">Ustanička 170</td><td class="txt-center">11050</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">104</td><td class="txt-center">Jurija Gagarina 30g</td><td class="txt-center">11070</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">109</td><td class="txt-center">Drage Spasić 32a</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">122</td><td class="txt-center">Uralska 3</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">123</td><td class="txt-center">Južni Bulevar 98</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">47</td><td class="txt-center">Partizanske avijacije 2</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">67</td><td class="txt-center">Makedonska 44</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">85</td><td class="txt-center">Masarikova 5</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">86</td><td class="txt-center">Rableova 17</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">96</td><td class="txt-center">Bulevar kralja Aleksandra 30-32</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">97</td><td class="txt-center">Knez Mihailova 54</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">111</td><td class="txt-center">Višnjićeva 8</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">114</td><td class="txt-center">Čika Ljubina 9</td><td class="txt-center">11000</td><td class="txt-center">Beograd</td></tr>
					 <tr class="table_row"><td class="txt-center">50</td><td class="txt-center">Dragiše Mišovića 144</td><td class="txt-center">32000</td><td class="txt-center">Čačak</td></tr>
					 <tr class="table_row"><td class="txt-center">32</td><td class="txt-center">Vojvode Stepe 11</td><td class="txt-center">22320</td><td class="txt-center">Inđija</td></tr>
					 <tr class="table_row"><td class="txt-center">66</td><td class="txt-center">Vuka Bojovića bb</td><td class="txt-center">35000</td><td class="txt-center">Jagodina</td></tr>
					 <tr class="table_row"><td class="txt-center">113</td><td class="txt-center">Kneginje Milice 10</td><td class="txt-center">35000</td><td class="txt-center">Jagodina</td></tr>
					 <tr class="table_row"><td class="txt-center">15</td><td class="txt-center">Trg srpskih dobrovoljaca 45</td><td class="txt-center">23000</td><td class="txt-center">Kikinda</td></tr>
					 <tr class="table_row"><td class="txt-center">38</td><td class="txt-center">Kralja Aleksandra I Karađorđević 32</td><td class="txt-center">34000</td><td class="txt-center">Kragujevac</td></tr>
					 <tr class="table_row"><td class="txt-center">116</td><td class="txt-center">Bulevar Kraljice Marije 56</td><td class="txt-center">34000</td><td class="txt-center">Kragujevac</td></tr>
					 <tr class="table_row"><td class="txt-center">61</td><td class="txt-center">Doktora Zorana Đinđića 13</td><td class="txt-center">34000</td><td class="txt-center">Kragujevac</td></tr>
					 <tr class="table_row"><td class="txt-center">11</td><td class="txt-center">Miloša Velikog 46</td><td class="txt-center">36000</td><td class="txt-center">Kraljevo</td></tr>
					 <tr class="table_row"><td class="txt-center">106</td><td class="txt-center">Dušana Popovića 39a</td><td class="txt-center">36000</td><td class="txt-center">Kraljevo</td></tr>
					 <tr class="table_row"><td class="txt-center">117</td><td class="txt-center">Krfska 5</td><td class="txt-center">37000</td><td class="txt-center">Kruševac</td></tr>
					 <tr class="table_row"><td class="txt-center">58</td><td class="txt-center">Blagoja Parovića 20</td><td class="txt-center">37000</td><td class="txt-center">Kruševac</td></tr>
					 <tr class="table_row"><td class="txt-center">71</td><td class="txt-center">Takovska 1</td><td class="txt-center">37000</td><td class="txt-center">Kruševac</td></tr>
					 <tr class="table_row"><td class="txt-center">99</td><td class="txt-center">Železnička 4</td><td class="txt-center">11550</td><td class="txt-center">Lazarevac</td></tr>
					 <tr class="table_row"><td class="txt-center">125</td><td class="txt-center">Bulevar oslobođenja bb</td><td class="txt-center">16000</td><td class="txt-center">Leskovac</td></tr>
					 <tr class="table_row"><td class="txt-center">43</td><td class="txt-center">Braće Badžak 2</td><td class="txt-center">11400</td><td class="txt-center">Mladenovac</td></tr>
					 <tr class="table_row"><td class="txt-center">44</td><td class="txt-center">Nikole Pašića 29</td><td class="txt-center">18000</td><td class="txt-center">Niš</td></tr>
					 <tr class="table_row"><td class="txt-center">55</td><td class="txt-center">Trg Kralja Milana 11</td><td class="txt-center">18000</td><td class="txt-center">Niš</td></tr>
					 <tr class="table_row"><td class="txt-center">68</td><td class="txt-center">Vizantijski Bulevar bb</td><td class="txt-center">18000</td><td class="txt-center">Niš</td></tr>
					 <tr class="table_row"><td class="txt-center">88</td><td class="txt-center">Bul. Mediana 21</td><td class="txt-center">18000</td><td class="txt-center">Niš</td></tr>
					 <tr class="table_row"><td class="txt-center">14</td><td class="txt-center">Rifata Burdževića bb</td><td class="txt-center">36300</td><td class="txt-center">Novi Pazar</td></tr>
					 <tr class="table_row"><td class="txt-center">112</td><td class="txt-center">Save Kovačevića bb</td><td class="txt-center">36300</td><td class="txt-center">Novi Pazar</td></tr>
					 <tr class="table_row"><td class="txt-center">31</td><td class="txt-center">Braće Ribnikar 29</td><td class="txt-center">21000</td><td class="txt-center">Novi Sad</td></tr>
					 <tr class="table_row"><td class="txt-center">36</td><td class="txt-center">Trg majke Jevrosime 21</td><td class="txt-center">21000</td><td class="txt-center">Novi Sad</td></tr>
					 <tr class="table_row"><td class="txt-center">10</td><td class="txt-center">Bulevar Oslobođenja 30</td><td class="txt-center">21000</td><td class="txt-center">Novi Sad</td></tr>
					 <tr class="table_row"><td class="txt-center">41</td><td class="txt-center">Bulevar Mihajla Pupina 2</td><td class="txt-center">21000</td><td class="txt-center">Novi Sad</td></tr>
					 <tr class="table_row"><td class="txt-center">59</td><td class="txt-center">Sent Andrejski put 11</td><td class="txt-center">21000</td><td class="txt-center">Novi Sad</td></tr>
					 <tr class="table_row"><td class="txt-center">70</td><td class="txt-center">Narodnog fronta 23b</td><td class="txt-center">21000</td><td class="txt-center">Novi Sad</td></tr>
					 <tr class="table_row"><td class="txt-center">83</td><td class="txt-center">Episkopa Visariona 2c</td><td class="txt-center">21000</td><td class="txt-center">Novi Sad</td></tr>
					 <tr class="table_row"><td class="txt-center">115</td><td class="txt-center">Bulevar Oslobođenja 119</td><td class="txt-center">21000</td><td class="txt-center">Novi Sad</td></tr>
					 <tr class="table_row"><td class="txt-center">120</td><td class="txt-center">Bulevar Evrope 26</td><td class="txt-center">21000</td><td class="txt-center">Novi Sad</td></tr>
					 <tr class="table_row"><td class="txt-center">64</td><td class="txt-center">Sutjeska 2</td><td class="txt-center">21000</td><td class="txt-center">Novi Sad</td></tr>
					 <tr class="table_row"><td class="txt-center">91</td><td class="txt-center">Vojvode Mišića 36v</td><td class="txt-center">11500</td><td class="txt-center">Obrenovac</td></tr>
					 <tr class="table_row"><td class="txt-center">49</td><td class="txt-center">Miloša Obrenovića 12</td><td class="txt-center">26000</td><td class="txt-center">Pančevo</td></tr>
					 <tr class="table_row"><td class="txt-center">94</td><td class="txt-center">Vojvode Radomira Putnika 8</td><td class="txt-center">26000</td><td class="txt-center">Pančevo</td></tr>
					 <tr class="table_row"><td class="txt-center">8</td><td class="txt-center">Kralja Petra 55</td><td class="txt-center">35000</td><td class="txt-center">Paraćin</td></tr>
					 <tr class="table_row"><td class="txt-center">100</td><td class="txt-center">Đure Đakovića bb</td><td class="txt-center">12000</td><td class="txt-center">Požarevac</td></tr>
					 <tr class="table_row"><td class="txt-center">82</td><td class="txt-center">Jovana Cvijica 1a</td><td class="txt-center">15000</td><td class="txt-center">Šabac</td></tr>
					 <tr class="table_row"><td class="txt-center">98</td><td class="txt-center">Šalinačka 58</td><td class="txt-center">11300</td><td class="txt-center">Smederevo</td></tr>
					 <tr class="table_row"><td class="txt-center">74</td><td class="txt-center">Despota Đurđa 15</td><td class="txt-center">11300</td><td class="txt-center">Smederevo</td></tr>
					 <tr class="table_row"><td class="txt-center">22</td><td class="txt-center">Kralja Petra I 15</td><td class="txt-center">25000</td><td class="txt-center">Sombor</td></tr>
					 <tr class="table_row"><td class="txt-center">90</td><td class="txt-center">Štrosmajerova 28</td><td class="txt-center">25000</td><td class="txt-center">Sombor</td></tr>
					 <tr class="table_row"><td class="txt-center">107</td><td class="txt-center">Bulevar Konstantina Velikog</td><td class="txt-center">22000</td><td class="txt-center">Sremska Mitrovica</td></tr>
					 <tr class="table_row"><td class="txt-center">102</td><td class="txt-center">Svetosavska 13-15</td><td class="txt-center">22300</td><td class="txt-center">Stara Pazova</td></tr>
					 <tr class="table_row"><td class="txt-center">77</td><td class="txt-center">Segedinski put 88a</td><td class="txt-center">24000</td><td class="txt-center">Subotica</td></tr>
					 <tr class="table_row"><td class="txt-center">62</td><td class="txt-center">Korzo 10</td><td class="txt-center">24000</td><td class="txt-center">Subotica</td></tr>
					 <tr class="table_row"><td class="txt-center">16</td><td class="txt-center">Dimitrija Tucovića 149</td><td class="txt-center">31000</td><td class="txt-center">Užice</td></tr>
					 <tr class="table_row"><td class="txt-center">89</td><td class="txt-center">Uzun Mirkova 2b</td><td class="txt-center">14000</td><td class="txt-center">Valjevo</td></tr>
					 <tr class="table_row"><td class="txt-center">53</td><td class="txt-center">Stefana Prvovenčanog 51</td><td class="txt-center">17500</td><td class="txt-center">Vranje</td></tr>
					 <tr class="table_row"><td class="txt-center">5</td><td class="txt-center">Trg Pobede 3</td><td class="txt-center">26300</td><td class="txt-center">Vršac</td></tr>
					 <tr class="table_row"><td class="txt-center">103</td><td class="txt-center">Miloša Obilića bb</td><td class="txt-center">26300</td><td class="txt-center">Vršac</td></tr>
					 <tr class="table_row"><td class="txt-center">119</td><td class="txt-center">Čupićeva 2</td><td class="txt-center">19000</td><td class="txt-center">Zaječar</td></tr>
					 <tr class="table_row"><td class="txt-center">81</td><td class="txt-center">Bagljaš - Zapad 5</td><td class="txt-center">23000</td><td class="txt-center">Zrenjanin</td></tr>
				</table>
		</div>
	</div>
</div>

@stop