<div id="tabela-vauceri">
<h5>Vaučeri</h5>
    <br/>
<table class="table">
    <thead>
    <tr class="table_head bg12">
        <th class="column-1 p-l-30"></th>
        <th class="column-2">Kod vaučera</th>
        <th class="column-3"></th>
        <th class="column-4"></th>
        <th class="column-5">Iznos</th>
    </tr>
    </thead>
    <tbody>
        @foreach($stavkeVauceri as $stavkaVaucer)
            <tr id="stavka-vaucer-{{$stavkaVaucer->rowId}}">
                @include('include.popupDialog', ['poruka' => 'Da li ste sigurni da želite da uklonite vaučer?', 'linkUspesno' => 'javascript:ukloniVaucer(\''. $stavkaVaucer->rowId . '\',\'' . $stavkaVaucer->id .'\')', 'dialogId' => 'vaucer-' . $stavkaVaucer->rowId])
                <td class="column-1">
                    <p>Vaučer</p>
                </td>
                <td class="column-2">
                    <div>{{$stavkaVaucer->name}}</div>
                </td>
                <td class="column-3">
                </td>
                <td class="column-4">
                </td>

                <td class="column-5" >
                    <div class="flex-w flex-sb-m">
                        <span >
                             -{{number_format($stavkaVaucer->price, 2, ',', '.')}} din.
                        </span>

                        <div class="fs-15 hov-cl10 pointer">
                            <span onclick="otvoriDialogSaId('vaucer-{!! $stavkaVaucer->rowId!!}')" class="lnr lnr-cross"></span>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
</div>
