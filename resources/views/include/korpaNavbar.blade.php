<!-- PRVO DIJALOGE ZA SVE STAVKE DA BI SE PRIKAZIVALI PREKO CELOG EKRANA -->
<div class="icon-header-item flex-c-m trans-04 icon-header-noti" data-notify="{{Cart::instance('korpa')->count()}}">
    <img src="/images/icons/icon-cart-3.png" alt="CART">
</div>

@if(Cart::instance('korpa')->count() == 0)
    <div class="cart-header menu-click-child trans-04">
        <div class="bo-b-1 bocl15">
            <div class="size-h-2 js-pscroll m-r--15 p-r-15">
                Korpa je prazna.
            </div>

        </div>
    </div>
@else

    @foreach(Cart::instance('korpa')->content() as $stavka)
        @include('include.popupDialog', ['poruka' => 'Da li ste sigurni da želite da obrišete proizvod iz korpe?', 'linkUspesno' => 'javascript:obrisiIzKorpe(\''. $stavka->rowId . '\',\'' . $stavka->id .'\')', 'dialogId' => 'korpaNavbar-' . $stavka->rowId])
    @endforeach

    <div class="cart-header menu-click-child trans-04">
        <div class="bo-b-1 bocl15">
            <div class="size-h-2 js-pscroll m-r--15 p-r-15">
            @foreach(Cart::instance('korpa')->content() as $stavka)
                <!-- cart header item -->
                    <div class="flex-w flex-str m-b-25">
                        <div class="size-w-15 flex-w flex-t">
                            @if(File::exists(public_path('/images/proizvodi/' .$stavka->id  . '/glavna/' . $stavka->nazivGlavneSlike . '.jpg')))
                                <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $stavka->name))?>/{{$stavka->id}}" class="wrap-pic-w bo-all-1 bocl12 size-w-16 hov3 trans-04 m-r-14">
                                    <img src="{{asset('images/proizvodi/'. $stavka->id .'/glavna/' . $stavka->nazivGlavneSlike . '.jpg')}}" alt="<?=str_replace('?', '',str_replace(' ', '-', $stavka->name))?>">
                                </a>
                            @endif

                            <div class="size-w-17 flex-col-l">
                                <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $stavka->name))?>/{{$stavka->id}}" class="txt-s-108 cl3 hov-cl10 trans-04">
                                    {{$stavka->name}}
                                </a>

                                <span class="txt-s-101 cl9">
                                                        {{number_format($stavka->price - $stavka->tax, 0, ',', '.')}} din.
                                                    </span>

                                <span class="txt-s-109 cl12">
                                                        x{{$stavka->qty}}
                                                    </span>
                            </div>
                        </div>

                        <div class="size-w-14 flex-b">
                            <a href="javascript:otvoriDialogSaId('korpaNavbar-{{$stavka->rowId}}')" class="lh-10">
                                <img src="/images/icons/icon-close.png" alt="CLOSE">
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <!--
                                            <div class="flex-w flex-sb-m p-t-22 p-b-12">
                                                <span class="txt-m-103 cl3 p-r-20">
                                                    Subtotal
                                                </span>

                                                <span class="txt-m-111 cl6">
                                                    48$
                                                </span>
                                            </div>
        -->
        <br/>
        <div class="flex-w flex-sb-m p-b-31">
            <span class="txt-m-103 cl3 p-r-20">
                Ukupno
            </span>

            <span class="txt-m-111 cl10">
                {{Cart::instance('korpa')->total(0,',', '.')}} din.
            </span>
        </div>

        <a href="/korpa" class="flex-c-m size-a-8 bg10 txt-s-105 cl13 hov-btn2 trans-04">
            Pregled korpe
        </a>
    </div>

    <!-- prvi put ce ovo da se uradi iz main.js kada je document ready, mora ovako inace bi prvi put ispisivalo gresku -->
    @if(Request::ajax())
        <script>
            namestiScrollbarZaKorpuNavbar();
        </script>
    @endif
@endif


