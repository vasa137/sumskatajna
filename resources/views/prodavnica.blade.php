@extends('layout')

@section('meta')
<meta property="og:title" content="ŠUMSKA TAJNA - PRODAVNICA" />
<meta property="og:description" content="ŠUMSKA TAJNA - PREMIUM FOOD " />
<meta property="og:image" content="http://sumskatajna.rs/images/og-slika.jpg" />
@stop


@section('sekcije')
@include('include.popupDialog', ['poruka' => 'Uspešno ste dodali proizvod u korpu. <br/> Da li želite da pregledate korpu?', 'linkUspesno' => '/korpa'])

	<!-- Title page -->
<section class="bg-img1" style="background-image: url(images/pozadina-heder-namazi-sa-pecurkama.jpg);">
	<div class="container">
		<div class="txt-center p-t-160 p-b-165">
			<h2 class="txt-l-101 cl0 txt-center p-b-14 respon1">
				PRODAVNICA
			</h2>

			<span class="txt-m-201 cl0 flex-c-m flex-w">
				<a href="/" class="txt-m-201 cl0 hov-cl10 trans-04 m-r-6">
					Naslovna
				</a>

				<span>
					/ Prodavnica
				</span>
			</span>
		</div>
	</div>
</section>
<div class="row " style=" text-align: center; width: 100%;">
    <div class="container">
        <h3><strong>BESPLATNA DOSTAVA ZA SVE PORDUŽBINE PREKO 1990 DIN.</strong> <br><br></h3>
    </div>  

</div>

<!--
<a href="/prodavnica">
    <img class="telefon" width="100%" style="float:left;" src="/images/novo/st-baner-brasno.jpg" alt="IMG-BG" class="rev-slidebg">
    <img class="komp" width="100%" style="float:left;" src="/images/novo/st-brasno-baner-za-telefon.jpg" alt="IMG-BG" class="rev-slidebg">
    </a>
-->
<!-- Content page -->
<div class="bg0 p-t-85 p-b-95">

	<div class="container">
		<!-- Shop grid -->
		
		<div class="shop-grid">


			
			<div class="row">
				<div class="flex-w flex-r-m p-b-45 flex-row-rev col-sm-12 col-md-12 col-lg-12 p-b-30">
					<div class="flex-w flex-m p-tb-8">
						<div class="rs1-select2 bg0 size-w-56 bo-all-1 bocl15 m-tb-7 m-r-15">
							<select class="js-select2" name="sort" onchange="location = this.value;">
								<option selected disabled>Izaberite kategoriju proizvoda</option>
								<option value="/prodavnica">Svi proizvodi</option>
								@foreach($kategorije as $k)
								<option value="/kategorija/{{$k->naziv}}/{{$k->id}}">{{$k->naziv}}</option>
								@endforeach
								
							</select>
							<div class="dropDownSelect2"></div>
						</div>
					</div>
				</div>
                @foreach($proizvodi as $proizvod)
				<!-- PROIZVOD POČETAK- -->
				<div class="col-sm-6 col-md-3 col-lg-3 p-b-30">
					<!-- Block1 -->
					<div class="block1">
                        <div  class="block1-bg wrap-pic-w bo-all-1 bocl12 hov3 trans-04">
                            @if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike .'.jpg')))
                                <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}">
                                    <img width="100%" src="/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg" alt="<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>">
                                </a>
                            @endif
                            <div   class="txt-center flex-col-c-m p-b-2">
                                <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}" class="txt-m-103 cl3 txt-center hov-cl10 trans-04 js-name-b1">
                                    {{$proizvod->naziv}}<br>
                                </a>

                                @if($proizvod->na_popustu)
                                <strong class="block1-content-more txt-m-112 cl5 p-t-21 trans-04">
                                    {{number_format($proizvod->cena_popust, 0, ',', '.')}} din.
                                </strong>
                                <span>
                                    <del>{{number_format($proizvod->cena, 0, ',', '.')}} din.</del>
                                </span>
                                @else
                                <strong class="block1-content-more txt-m-112 cl5 p-t-21 trans-04">
                                    {{number_format($proizvod->cena, 0, ',', '.')}} din.
                                </strong>
                                @endif

                                <div class="block1-wrap-icon flex-c-m flex-w trans-05">
                                    <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}" class="block1-icon flex-c-m wrap-pic-max-w">
                                        <img src="/images/icons/icon-view.png" alt="ICON">
                                    </a>

                                    <a href="javascript: dodajUKorpu('{!! $proizvod->id !!}', 1);" class="block1-icon flex-c-m wrap-pic-max-w">
                                        <img src="/images/icons/icon-cart.png" alt="ICON">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
				<!-- PROIZVOD KRAJ- -->
                @endforeach



			</div>
		</div>
	</div>
</div>





@stop