<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title') Šumska Tajna - Namazi sa pecurkama, vegan, organsko, vegeterijansko</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
<!--===============================================================================================-->  

    <link rel="shortcut icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/linearicons-v1.0.0/icon-font.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/animate/animate.css')}}">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/animsition/css/animsition.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/daterangepicker/daterangepicker.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/slick/slick.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/lightbox2/css/lightbox.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/revolution/css/layers.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/revolution/css/navigation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/revolution/css/settings.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/noui/nouislider.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/slide100/slide100.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" href="{{asset('css/popupDialog.css')}}"/>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400" rel="stylesheet">


    @yield('meta')



    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117964354-10"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-117964354-10');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '579788342762258');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=579788342762258&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <style >
        
        .header-top-black {
              position: fixed; 
              top: 0; 
              width: 100%; 
              padding-top: 6px;
              
              background: black;
              color: white;
              text-align: center;
            }
    
        .telefon {
            display:none;
        }

        .komp{
            display:block;
        }
     
        @media only screen and (min-device-width:600px){
            
            .telefon {
            display:block;
            }
            
            .komp{
            display:none;
        }

        }
        
        

    </style>
    @yield('scriptsTop')
</head>
<body class="animsition">

    <!-- Header -->
    <header class="header-v1" >
        
        <!-- Header desktop -->
        <div class="container-menu-desktop">

            <div class="wrap-menu-desktop">
                    <div class="header-top-black">
                        <p style="color: white;">BESPLATNA DOSTAVA ZA SVE PORDUŽBINE PREKO 1990 DIN.</p>
                    </div>
                <nav class="limiter-menu-desktop" style="margin-top: 25px;">
                    
                    <div class="left-header">

                        <!-- Menu desktop -->
                        <div class="menu-desktop" >

                            <ul class="main-menu">
                                <li class="active-menu">
                                    <a href="/prodavnica">PRODAVNICA</a>
                                    <ul class="sub-menu">
                                        <li><a href="/prodavnica">Svi proizvodi</a></li>
                                        <li><a href="/kategorija/Namazi sa pečurkama/1">Namazi sa pečurkama</a></li>
                                        <li><a href="/kategorija/Humusi/3">Humusi</a></li>
                                        <li><a href="/kategorija/Voćni namazi/2">Voćni namazi</a></li>
                                        <li><a href="/kategorija/Med/8">Med</a></li>
                                        <li><a href="/kategorija/Čajevi/4">Čajevi</a></li>
                                        <li><a href="/kategorija/Brašna/5">Brašna</a></li>
                                        <li><a href="/kategorija/Leblebija/6">Leblebija</a></li>
                                        <li><a href="/kategorija/Bez-Glutena-Gluten-Free/10">Bez Glutena / Gluten Free</a></li>
                                        <li><a href="/kategorija/Bez-sećera-pogodno-za-dijabeticare/11">Bez Šećera / Pogodno za Dijabetičare</a></li>
                                        <li><a href="/kategorija/Vegan/9">Vegan</a></li>
                                        <li><a href="/kategorija/AKCIJE I SETOVI/7">AKCIJE I SETOVI</a></li>
                                       
                                    </ul>
                                </li>

                                <li>
                                    <a href="/sumska-prica">ŠUMSKA PRIČA</a>
                                    
                                </li>
                                
                                <li>
                                    <a href="/recepti">RECEPTI</a>
                                    
                                </li>
                            
                                <li>
                                    <a href="/blog">BLOG</a>
                                </li>
                                <li>
                                    <a href="/kontakt">KONTAKT</a>
                                    
                                </li>

                                

                                
                            </ul>
                        </div>  
                    </div>
                        
                    <div class="center-header">
                        <!-- Logo desktop -->       
                        <div class="logo">
                        <a href="/"><img src="{{asset('images/logo.png')}}" alt="IMG-LOGO"></a>
                        </div>
                    </div>


                    <!-- Icon header -->
                    <div class="right-header">
                        <div class="wrap-icon-header flex-w flex-r-m h-full wrap-menu-click p-t-8">
                            <a  target="_blank" rel="nofollow" href="http://forest-secret.com/"><img style="height: 22px;" src="{{asset('images/eng.png')}}"></a>
                            @if(Route::getCurrentRoute()->uri != 'korpa')
                                <div id="korpaInclude"  class="wrap-cart-header h-full flex-m m-l-10 menu-click">
                                    @include('include.korpaNavbar')
                                </div>
                            @endif

                        </div>
                    </div>

                </nav>
            </div>  
        </div>

        <!-- Header Mobile -->
        <div class="wrap-header-mobile">
            <!-- Logo moblie -->        
            

            <div class="logo-mobile">
                <a href="/"><img src="{{asset('images/logo.png')}}" alt="IMG-LOGO"></a>
            </div>

            <!-- Icon header -->
            <div class="wrap-icon-header flex-w flex-r-m h-full wrap-menu-click m-r-15">
                @if(Route::getCurrentRoute()->uri != 'korpa')
                    <div id="korpaInclude"  class="wrap-cart-header h-full flex-m m-l-10 menu-click">
                        @include('include.korpaNavbar')
                    </div>
                @endif
                
            </div>

            <!-- Button show menu -->
            <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </div>
        </div>


        <!-- Menu Mobile -->
        <div class="menu-mobile">
            <ul class="main-menu-m">
               <li>
                    <a onclick="document.getElementById('klikProdavnica').click()">PRODAVNICA</a>
                    <ul class="sub-menu-m">
                        <li><a href="/prodavnica">Svi proizvodi</a></li>
                        <li><a href="/kategorija/Namazi sa pečurkama/1">Namazi sa pečurkama</a></li>
                        <li><a href="/kategorija/Humusi/3">Humusi</a></li>
                        <li><a href="/kategorija/Voćni namazi/2">Voćni namazi</a></li>
                        <li><a href="/kategorija/Med/8">Med</a></li>
                        <li><a href="/kategorija/Čajevi/4">Čajevi</a></li>
                        <li><a href="/kategorija/Brašna/5">Brašna</a></li>
                        <li><a href="/kategorija/Leblebija/6">Leblebija</a></li>
                        <li><a href="/kategorija/Bez-Glutena-Gluten-Free/10">Bez Glutena / Gluten Free</a></li>
                        <li><a href="/kategorija/Bez-sećera-pogodno-za-dijabeticare/11">Bez Šećera / Pogodno za Dijabetičare</a></li>
                        <li><a href="/kategorija/Vegan/9">Vegan</a></li>
                        <li><a href="/kategorija/AKCIJE I SETOVI/7">AKCIJE I SETOVI</a></li>
                       
                    </ul>
                    <span id="klikProdavnica" class="arrow-main-menu-m">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </span>
                </li>

                <li>
                    <a href="/sumska-prica">ŠUMSKA PRIČA</a>
                    
                </li>

                <li>
                    <a href="/recepti">RECEPTI</a>
                    
                </li>
            
                <li>
                    <a href="/blog">BLOG</a>
                </li>
                <li>
                    <a href="/kontakt">KONTAKT</a>
                    
                </li>
            </ul>
        </div>

        <!-- Modal Search -->
        <div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
            <button class="flex-c-m btn-hide-modal-search trans-04 js-hide-modal-search">
                <span class="lnr lnr-cross"></span>
            </button>
            
            <div class="container-search-header">
                <form class="wrap-search-header flex-w">
                    <button class="flex-c-m trans-04">
                        <span class="lnr lnr-magnifier"></span>
                    </button>
                    <input class="plh1" type="text" name="search" placeholder="Search...">
                </form>
            </div>
        </div>
    </header>
    



    @yield('sekcije')
    <!-- Slider -->
    

    <!-- Footer -->
    <footer style="background-color: black; ">
        <div class="container">
            
            <div class="wrap-footer flex-w p-t-60 p-b-62">
                <div class="footer-col1">
                    <div class="footer-col-title">
                        <a href="#">
                            <img height="120" src="{{asset('images/logo.png')}}" alt="LOGO">
                        </a>
                    </div>

                    <p class="txt-s-101 cl6 size-w-10 p-b-16" style="color: white;">
                        <br>
                        Od Decembra 2019 godine pored namaza sa pečurkama u našoj paleti proizvoda se mogu naći i voćni namazi, humusi, brašna, čajevi kao i mnogi drugi proizvodi organskog porekla i neprskanih sirovina.
                    </p>

                    
                </div>

                <div class="footer-col2">
                    <div class="footer-col-title flex-m">
                        <span class="txt-m-109 cl3" style="color: white;">
                            Navigacija
                        </span>
                    </div>
                    
                    <ul>
                        <li class="p-b-16">
                            <a style="color: white;" href="/prodavnica" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">
                                Prodavnica
                            </a>
                        </li>

                        <li class="p-b-16">
                            <a style="color: white;" href="/sumska-prica" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">
                                Šumska priča
                            </a>
                        </li>
<!--
                        <li class="p-b-16">
                            <a style="color: white;" href="/recepti" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">
                                Recepti
                            </a>
                        </li>
-->
                        <li class="p-b-16">
                            <a style="color: white;" href="/blog" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">
                                Blog
                            </a>
                        </li>

                        <li class="p-b-16">
                            <a style="color: white;" href="/kontakt" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">
                                Kontakt
                            </a>
                        </li>
                    </ul>
                </div>







                <div class="footer-col3">
                   <div class="footer-col-title flex-m">
                        <span style="color: white;" class="txt-m-109 cl3">
                             Prodavnica
                        </span>
                    </div>

                    <ul>
                        <li ><a style="color: white;" href="/prodavnica" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">Svi proizvodi</a></li>
                        <li><a style="color: white;" href="/kategorija/Namazi sa pečurkama/1" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">Namazi sa pečurkama</a></li>
                        <li><a style="color: white;" href="/kategorija/Humusi/3" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">Humusi</a></li>
                        <li><a style="color: white;" href="/kategorija/Voćni namazi/2" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">Voćni namazi</a></li>
                        <li><a style="color: white;" href="/kategorija/Med/8" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">Med</a></li>
                        <li><a style="color: white;" href="/kategorija/Čajevi/4" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">Čajevi</a></li>
                        <li><a style="color: white;" href="/kategorija/Brašna/5" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">Brašna</a></li>
                        <li><a style="color: white;" href="/kategorija/Leblebija/6" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">Leblebija</a></li>

                        <li><a style="color: white;" href="/kategorija/Bez-Glutena-Gluten-Free/10" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">Bez Glutena / Gluten Free</a></li>
                        <li><a style="color: white;" href="/kategorija/Bez-sećera-pogodno-za-dijabeticare/11" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">Bez Šećera / Pogodno za Dijabetičare</a></li>
                        <li><a style="color: white;" href="/kategorija/Vegan/9" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">Vegan</a></li>
                        <li><a style="color: white;" href="/kategorija/AKCIJE I SETOVI/7" class="txt-s-101 cl6 hov-cl10 trans-04 p-tb-5">AKCIJE I SETOVI</a></li>
                    </ul>
                </div>


            


                <div class="footer-col4">
                     <div class="footer-col-title flex-m">
                        <span class="txt-m-109 cl3" style="color: white;">
                            Informacije
                        </span>
                    </div>
                    <ul>
                        <li class="txt-s-101 cl6 flex-t p-b-10">
                            <span class="size-w-11">
                                <img src="{{asset('images/icons/icon-mail.png')}}" alt="ICON-MAIL">
                            </span>
                            
                            <span style="color: white;" class="size-w-12 p-t-1">
                                info@sumskatajna.rs
                            </span>
                        </li>

                        <li class="txt-s-101 cl6 flex-t p-b-10">
                            <span class="size-w-11">
                                <img src="{{asset('images/icons/icon-pin.png')}}" alt="ICON-MAIL">
                            </span>
                            
                            <span style="color: white;" class="size-w-12 p-t-1">
                                Dunavska 70, 21203 Vetrenik
                            </span>
                        </li>

                        <li class="txt-s-101 cl6 flex-t p-b-10">
                            <span class="size-w-11">
                                <img src="{{asset('images/icons/icon-phone.png')}}" alt="ICON-MAIL">
                            </span>
                            
                            <span style="color: white;" class="size-w-12 p-t-1">
                                +381 61 600 4919
                            </span>
                        </li>
                        <li>
                            <a rel="nofollow" target="_blank" href="https://www.instagram.com/sumska.tajna/" >
                                <img style="float: left;
                                  width: 20%;
                                  padding: 5px;" src="{{asset('/images/instagram-sumska-tajna.png')}}">
                            </a>
                            <a rel="nofollow" target="_blank" href="https://www.facebook.com/sumska.tajna1/?view_public_for=113196212839598" >
                                <img style="float: left;
                                  width: 20%;
                                  padding: 5px;" src="{{asset('/images/facebook-sumska-tajna.png')}}">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="flex-w flex-sb-m bo-t-1 bocl14 p-tb-14">
                <span style="color: white;" class="txt-s-101 cl9 p-tb-10 p-r-29">
                    Copyright © 2019 Šumska Tajna. All rights reserved.
                </span>
            </div>
        </div>
    </footer>
    

    <!-- Back to top -->
    <div class="btn-back-to-top bg0-hov" id="myBtn">
        <span class="symbol-btn-back-to-top">
            <span class="lnr lnr-chevron-up"></span>
        </span>
    </div>

    







<!--===============================================================================================-->  
    <script src="{{asset('vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('vendor/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('vendor/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{asset('vendor/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{asset('vendor/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
    <script src="{{asset('vendor/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script src="{{asset('vendor/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script src="{{asset('vendor/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script src="{{asset('vendor/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script src="{{asset('vendor/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script src="{{asset('vendor/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script src="{{asset('vendor/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script src="{{asset('vendor/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script src="{{asset('js/revo-custom.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('vendor/daterangepicker/moment.min.js')}}"></script>
    <script src="{{asset('vendor/daterangepicker/daterangepicker.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('vendor/slick/slick.min.js')}}"></script>
    <script src="{{asset('js/slick-custom.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('vendor/parallax100/parallax100.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('vendor/lightbox2/js/lightbox.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('vendor/isotope/isotope.pkgd.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('vendor/sweetalert/sweetalert.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('vendor/countdowntime/moment.min.js')}}"></script>
    <script src="{{asset('vendor/countdowntime/moment-timezone.min.js')}}"></script>
    <script src="{{asset('vendor/countdowntime/moment-timezone-with-data.min.js')}}"></script>
    <script src="{{asset('vendor/countdowntime/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{asset('vendor/countterup/jquery.counterup.min.js')}}"></script>

    <script src="{{asset('vendor/noui/nouislider.min.js')}}"></script>
    <!--===============================================================================================-->


    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/slick-slider.js')}}"> </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <script src="{{asset('js/korpaNavbarScrollbar.js')}}"></script>
    <script src="{{asset('js/popupDialog.js')}}"></script>
    <script src="{{asset('js/klijentKorpa.js')}}"></script>
    @yield('scriptsBottom')
</body>

</html>