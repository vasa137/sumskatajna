<!DOCTYPE html >
<html>

<head>
	<meta name="viewport" content="width=device-width" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<link rel="stylesheet" type="text/css" href="http://sumskatajna.rs/css/mailTemplate.css" />
	<link rel="stylesheet" type="text/css" href="http://sumskatajna.rs/css/bootstrap.css">

</head>

<body style="background-color: black;">

<!-- HEADER -->
<div class="container">



	<!-- BODY -->
	<table class="body-wrap">
		<tr>
			<td></td>
			<td class="container" style="background-color: black; color: white;" >

				<div class="content">
					<table>
						<tr>
							<td>


								<!-- You may like to include a Hero Image -->
								<p><img src="http://sumskatajna.rs/images/logo.png" style="width: 120px;height:auto;" /></p>
								<!-- /Hero Image -->

								<br/>

								<h4>Potvrda porudžbine #{{$porudzbina->id}} <small> {{ date('d.m.Y - H:i', strtotime($vreme)) }}</small></h4>
								<p>Poštovani, Vaša porudžbina je uspešno kreirana i biće isporučena u roku od 2 do 3 radna dana.</p>

								<br/>


							@foreach($stavke as $s)
								<!-- Line Items -->
									<div class="products">
										<img src="http://sumskatajna.rs/images/proizvodi/' . $s->id_proizvod . '/glavna/' . $glavneSlike[$s->id_proizvod]. '.jpg'" style="width:140px; height:auto;" />
										<span> {{$s->kolicina}}x {{$proizvodi[$s->id]->naziv}}
											@if($s->id_kupon != null)
												(kupon: {{$kuponi[$s->id_kupon]->kod}} )
											@endif

											@if($s->cena_popust < $s->cena)
												<del>{{number_format($s->cena, 0, ',', '.')}}</del>
												{{number_format($s->cena_popust, 0, ',', '.')}}
											@else
												{{number_format($s->cena_popust, 0, ',', '.')}}
											@endif


								= {{number_format($s->ukupno_popust, 0, ',', '.')}} rsd
								</span>
									</div>
									<hr>
									<div style="clear:both;"></div>
							@endforeach
							<!-- /Line Items -->

								<br/>
								<br/>
								@if($porudzbina->ima_vaucere)
									@foreach($vauceri as $vaucer)
										<h4>Vaučer {{$vaucer->kod}}: -{{number_format($vaucer->iznos, 0, ',', '.')}} rsd</h4>
								@endforeach
							@endif
							<!-- Totals -->
								<h4><b>Cena:</b> {{number_format($porudzbina->iznos, 0, ',', '.')}} rsd</h4>

								<!-- Totals -->
								<table class="columns" width="100%">
									<tr>
										<td>


											<!--- column 1 -->
											<table align="left" class="column">
												<tr>
													<td>
														<p><b>Dostava:</b>
															@if($porudzbina->iznos_popust < 1990)
															310 rsd
															@else
															besplatna
															@endif

														</p>
													</td>
												</tr>
												@if($porudzbina->iznos-$porudzbina->iznos_popust > 0)
												<tr>

													<td>
														<p><b>Popust:</b> {{number_format($porudzbina->iznos-$porudzbina->iznos_popust, 0, ',', '.')}} rsd </p>
													</td>
												</tr>
												@endif
											</table>
											<!-- /column 1 -->
											

										</td>
									</tr>
								</table>
								<!-- /Totals -->


								<h4><b>Ukupno: 

									@if($porudzbina->iznos_popust < 1990)
									{{number_format($porudzbina->iznos_popust, 0, ',', '.')}} rsd (+310 dostava)
									@else
									{{number_format($porudzbina->iznos_popust, 0, ',', '.')}} rsd
									@endif

								</b></h4>
								<!-- /Totals -->

								<br/>

								<!-- address detals -->
								<table class="columns" width="100%">
									<tr>
										<td>

											<!--- column 1 -->
											<table align="left" class="column">
												<tr>
													<td>
														<h5 class="">Podaci za dostavu</h5>
														<p class="">
															{{$porudzbina->kupac}}<br/>
															{{$porudzbina->adresa}}<br/>
															{{$porudzbina->zip}}, {{$porudzbina->grad}}<br/>
															{{$porudzbina->telefon}}<br/>
															{{$porudzbina->email}}<br>
															<br>
															<strong>Napomena:</strong><br>
															{{$porudzbina->napomena}}<br>
														</p>
													</td>
												</tr>
											</table>
											<!-- /column 1 -->




											<span class="clear"></span>

										</td>
									</tr>
								</table>
								<!-- /address details -->

								<br/>

								

								<br/>


							</td>
						</tr>
					</table>
				</div>

			</td>
			<td></td>
		</tr>
	</table>
	<!-- /BODY -->

	<!-- FOOTER -->
	<table class="footer-wrap" bgcolor="">
		<tr>
			<td></td>
			<td class="container">

				<!-- content -->
				<div class="content">
					<table>
						<tr>
							<td align="center">
								<a href="/" target="_blank"><img src="http://sumskatajna.rs/images/logo.png" style=" width:140px; height:auto" alt="/" /></a>
								<br/><br/>
								<p><strong><a href="mailto:info@sumskatajna.rs" style="color:white;">info@sumskatajna.rs</a></strong></p>
							</td>
						</tr>
					</table>
				</div><!-- /content -->

			</td>
			<td></td>
		</tr>
	</table><!-- /FOOTER -->
</div>

</body>
</html>