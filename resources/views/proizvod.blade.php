@extends('layout')

@section('scriptsTop')
	<!--Owl carosul-->
	<link href="{{asset('css/slick-slider-addons.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.default.css')}}">
	<link  rel="stylesheet"  href="{{asset('assets/js/plugins/slick/slick-theme.css')}}"/>
	<link href="{{asset('css/slick-lightbox.css')}}" rel="stylesheet" />

@endsection

@section('scriptsBottom')
	<!-- Owl carosul js -->
	<script src="{{asset('js/owl.carousel.min.js')}}"></script>
	<script src="{{asset('js/slick.min.js')}}"></script>
	<script src="{{asset('js/slick-lightbox.js')}}"></script>
	<script src="{{asset('js/klijentProizvod.js')}}"></script>
	<script>
		inicijalizujSlike();
	</script>
@endsection



@section('meta')
<meta property="og:title" content="ŠUMSKA TAJNA - PRODAVNICA - {{$proizvod->naziv}}" />
<meta property="og:description" content="ŠUMSKA TAJNA - {{$proizvod->naziv}} " />
<meta property="og:image" content="http://sumskatajna.rs/images/og-slika.jpg" />
@stop




@section('sekcije')
@include('include.popupDialog', ['poruka' => 'Uspešno ste dodali proizvod u korpu. <br/> Da li želite da pregledate korpu?', 'linkUspesno' => '/korpa'])
<!-- Title page -->
<section class="how-overlay2 bg-img1" style="background-image: url(/images/pozadina-heder-namazi-sa-pecurkama-crni.jpg);">

	<div class="container">
		<div class="row komp" style=" text-align: center;">
                <p style="color: white;"><strong>BESPLATNA DOSTAVA ZA SVE PORDUŽBINE PREKO 1990 DIN.</strong> <br><br></p>
            </div>
		<div class="txt-center p-t-160 p-b-165">
			<h2 class="txt-l-101 cl0 txt-center p-b-14 respon1">
				{{$proizvod->naziv}}
			</h2>

			<span class="txt-m-201 cl0 flex-c-m flex-w">
				<a href="/" class="txt-m-201 cl0 hov-cl10 trans-04 m-r-6">
					Naslovna
				</a>

				<a href="/prodavnica" class="txt-m-201 cl0 hov-cl10 trans-04 m-r-6">
					/ Prodavnica
				</a>

				<span>
					/ {{$proizvod->naziv}}
				</span>
			</span>
		</div>
	</div>
</section>

<!-- Product detail -->
<section class="telefon sec-product-detail p-t-50 p-b-70"  style=" background-color: black;">
	<div class="container">

		<div class="row">

			<div class="col-md-7 col-lg-5">
				<div class="m-r--30 m-r-0-lg">

					<!-- Slide 100 -->
					<div id="slide100-01">
						<div class="" id="product-details-slider" data-slider-id="1">
							@if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')))
								<div class="single-product-thumb">
									<a href="/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg"><img class="img-fluid" src="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')}}" alt="{{$proizvod->nazivGlavneSlike}}"></a>
								</div>
							@endif
						</div>
						
					</div>
				</div>
			</div>

			<div class="col-md-5 col-lg-7">
				<div class="p-l-70 p-t-35 p-l-0-lg">
					<h4 style="color:white;" class="js-name1 txt-l-104 cl3 p-b-16">
						{{$proizvod->naziv}}
					</h4>
					
					<span style="color:white;" class="txt-m-117 cl9">
						@if($proizvod->na_popustu)
							<strong>{{number_format($proizvod->cena_popust, 0, ',', '.')}} din.</strong> <del>{{number_format($proizvod->cena, 0, ',', '.')}} din.</del><br>
						@else
							<strong>{{number_format($proizvod->cena, 0, ',', '.')}} din.</strong><br>
						@endif
					</span>
					
					
					<p style="color:white;" class="txt-s-101 cl6">
						<strong>Neto: {{$proizvod->neto}}</strong>
						<br><br>
					</p>

					<p style="color:white;" class="txt-s-101 cl6">
						{{$proizvod->opis}}
						<br><br>
					</p>

					@if($proizvod->kratak_opis)
					<p style="color:white;" class="txt-s-101 cl6">
						<strong>Sastav:</strong> {{$proizvod->kratak_opis}}
						<br><br>
					</p>
					@endif

					@if($proizvod->upotreba)
					<p style="color:white;" class="txt-s-101 cl6">
						<strong>Upotreba:</strong> {{$proizvod->upotreba}}
						<br><br>
					</p>
					@endif

					@if($proizvod->cuvanje)
					<p style="color:white;" class="txt-s-101 cl6">
						<strong>Mesto čuvanja:</strong> {{$proizvod->cuvanje}}
						<br><br>
					</p>
					@endif

				

					@if($proizvod->ima_tagove)
					<div style="display: inline-block" class="txt-s-107 p-t-20 p-b-6">
						@foreach($proizvod->tagovi as $tag)
							@switch($tag->naziv)
							    @case("posno")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/namazi-sa-pecurkama-posno-2.png')}}">
							        @break
							    @case("vegan")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/namazi-sa-pecurkama-vegan-2.png')}}">
							        @break
							    @case("pogodno za dijabeticare")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/namazi-sa-pecurkama-dijabeticari-2.png')}}">
							         <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/bez-secera.png')}}">
							        @break
							    @case("neprskano")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/namazi-sa-pecurkama-neprskano-2.png')}}">
							        @break
							    @case("bez vestackih boja")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/namazi-sa-pecurkama-bez-vestackih-boja-2.png')}}">
							        @break
							    @case("gluten free")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/namazi-sa-pecurkama-bez-glutena-2.png')}}">
							        @break
							    @case("visoko proteinsko")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/visoko-proteinsko.png')}}">
							        @break
							    @case("nisko kaloricno")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/nisko-kaloricno.png')}}">
							        @break
						        @case("organsko")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/organsko.png')}}">
							        @break
							@endswitch
						@endforeach
					</div>
					@endif

					<div  class="flex-w flex-m p-t-5 p-b-30">
						<div class="wrap-num-product flex-w flex-m bg12 p-rl-10 m-r-30 m-b-30">
							<div class="btn-num-product-down flex-c-m fs-29"></div>

							<input id="kolicina" type="number" min="1" class="txt-m-102 cl6 txt-center num-product" name="num-product" value="1">

							<div class="btn-num-product-up flex-c-m fs-16"></div>
						</div>

						<a href="javascript:dodajUKorpu('{!! $proizvod->id !!}', $('#kolicina').val())" class="flex-c-m txt-s-103 cl0 bg10 size-a-2 hov-btn2 trans-04 m-b-30">
							Dodaj u korpu
						</a>
					</div>

					@if($proizvod->ima_tagove)
					<div class="txt-s-107 p-b-6">
						<span class="cl6">
							Tagovi:
						</span>

						@foreach($proizvod->tagovi as $tag)
						<span class="txt-s-107 cl9 hov-cl10 trans-04">
							{{$tag->naziv}}
							@if($tag != $proizvod->tagovi[count($proizvod->tagovi) - 1])
								,
							@endif
						</span>
						@endforeach
					</div>
					@endif
					<!--
					<div class="txt-s-107 p-b-6">
						<span class="cl6">
							Sku:
						</span>

						<span class="cl9">
							156
						</span>
					</div>

					<div class="txt-s-107 p-b-6">
						<span class="cl6">
							Category:
						</span>

						<span class="cl9">
							Fruit
						</span>
					</div>
					-->
					
				</div>
			</div>
		</div>

	</div>
</section>

<section class="komp sec-product-detail p-t-50 p-b-70"  style=" background-color: black;">
	<div class="container">
		<div class="row komp" style=" text-align: center;">
                <p style="color: white;"><strong>BESPLATNA DOSTAVA ZA SVE PORDUŽBINE PREKO 1990 DIN.</strong> <br><br></p>
            </div>
		<div class="row">
			<div class="col-md-6 col-lg-5">
				<div class="m-r--30 m-r-0-lg">
					<!-- Slide 100 -->
					<div id="slide100-01">
						<div class="" id="product-details-slider" data-slider-id="1">
							@if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')))
								<div class="single-product-thumb">
									<a href="/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg"><img class="img-fluid" src="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')}}" alt="{{$proizvod->nazivGlavneSlike}}"></a>
								</div>
							@endif
						</div>
						
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-7">
				<div class="p-l-70 p-t-35 p-l-0-lg">
					<h4 style="color:white;" class="js-name1 txt-l-104 cl3 p-b-16">
						{{$proizvod->naziv}}
					</h4>
					<!--
					<span style="color:white;" class="txt-m-117 cl9">
						@if($proizvod->na_popustu)
							<strong>{{number_format($proizvod->cena_popust, 0, ',', '.')}} din.</strong> <del>{{number_format($proizvod->cena, 0, ',', '.')}} din.</del><br>
						@else
							<strong>{{number_format($proizvod->cena, 0, ',', '.')}} din.</strong><br>
						@endif
					</span>

					<img width="50%" src="{{asset('/images/prirodni-vegan-namazi-ikonice/prirodni-namazi-besplatna-dostava.png')}}">
					-->
					
					<span style="color:white;" class="txt-m-117 cl9">
						@if($proizvod->na_popustu)
							<strong>{{number_format($proizvod->cena_popust, 0, ',', '.')}} din.</strong> <del>{{number_format($proizvod->cena, 0, ',', '.')}} din.</del><br>
						@else
							<strong>{{number_format($proizvod->cena, 0, ',', '.')}} din.</strong><br>
						@endif
					</span>
					
					
					<p style="color:white;" class="txt-s-101 cl6">
						<strong>Neto: {{$proizvod->neto}}</strong>
						<br><br>
					</p>

					<p style="color:white;" class="txt-s-101 cl6">
						{{$proizvod->opis}}
						<br><br>
					</p>

					@if($proizvod->kratak_opis)
					<p style="color:white;" class="txt-s-101 cl6">
						<strong>Sastav:</strong> {{$proizvod->kratak_opis}}
						<br><br>
					</p>
					@endif

					@if($proizvod->upotreba)
					<p style="color:white;" class="txt-s-101 cl6">
						<strong>Upotreba:</strong> {{$proizvod->upotreba}}
						<br><br>
					</p>
					@endif

					@if($proizvod->cuvanje)
					<p style="color:white;" class="txt-s-101 cl6">
						<strong>Mesto čuvanja:</strong> {{$proizvod->cuvanje}}
						<br><br>
					</p>
					@endif
					

					<div  class="flex-w flex-m p-t-5 p-b-30">
						<div class="wrap-num-product flex-w flex-m bg12 p-rl-10 m-r-30 m-b-30">
							<div class="btn-num-product-down flex-c-m fs-29"></div>

							<input id="kolicina" type="number" min="1" class="txt-m-102 cl6 txt-center num-product" name="num-product" value="1">

							<div class="btn-num-product-up flex-c-m fs-16"></div>
						</div>

						<a href="javascript:dodajUKorpu('{!! $proizvod->id !!}', $('#kolicina').val())" class="flex-c-m txt-s-103 cl0 bg10 size-a-2 hov-btn2 trans-04 m-b-30">
							Dodaj u korpu
						</a>
					</div>
					
					@if($proizvod->ima_tagove)
					<div style="display: inline-block" class="txt-s-107 p-t-20 p-b-6">
						@foreach($proizvod->tagovi as $tag)
							@switch($tag->naziv)
							    @case("posno")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/namazi-sa-pecurkama-posno-2.png')}}">
							        @break
							    @case("vegan")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/namazi-sa-pecurkama-vegan-2.png')}}">
							        @break
							    @case("pogodno za dijabeticare")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/namazi-sa-pecurkama-dijabeticari-2.png')}}">
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/bez-secera.png')}}">
							        @break
							    @case("neprskano")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/namazi-sa-pecurkama-neprskano-2.png')}}">
							        @break
							    @case("bez vestackih boja")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/namazi-sa-pecurkama-bez-vestackih-boja-2.png')}}">
							        @break
							    @case("gluten free")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/namazi-sa-pecurkama-bez-glutena-2.png')}}">
							        @break
							    @case("visoko proteinsko")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/visoko-proteinsko.png')}}">
							        @break
						        @case("nisko kaloricno")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/nisko-kaloricno.png')}}">
							        @break
							     @case("organsko")
							        <img style="float:left;" height="90" src="{{asset('/images/prirodni-vegan-namazi-ikonice/organsko.png')}}">
							        @break
							@endswitch
						@endforeach
					</div>
					@endif
					
<!--
					<div  class="flex-w flex-m p-t-5 p-b-30">
						<div class="wrap-num-product flex-w flex-m bg12 p-rl-10 m-r-30 m-b-30">
							<div class="btn-num-product-down flex-c-m fs-29"></div>

							<input id="kolicina" type="number" min="1" class="txt-m-102 cl6 txt-center num-product" name="num-product" value="1">

							<div class="btn-num-product-up flex-c-m fs-16"></div>
						</div>

						<a href="javascript:dodajUKorpu('{!! $proizvod->id !!}', $('#kolicina').val())" class="flex-c-m txt-s-103 cl0 bg10 size-a-2 hov-btn2 trans-04 m-b-30">
							Dodaj u korpu
						</a>
					</div>

					@if($proizvod->ima_tagove)
					<div class="txt-s-107 p-b-6">
						<span class="cl6">
							Tagovi:
						</span>

						@foreach($proizvod->tagovi as $tag)
						<span class="txt-s-107 cl9 hov-cl10 trans-04">
							{{$tag->naziv}}
							@if($tag != $proizvod->tagovi[count($proizvod->tagovi) - 1])
								,
							@endif
						</span>
						@endforeach
					</div>
					@endif
					-->
					<!--
					<div class="txt-s-107 p-b-6">
						<span class="cl6">
							Sku:
						</span>

						<span class="cl9">
							156
						</span>
					</div>

					<div class="txt-s-107 p-b-6">
						<span class="cl6">
							Category:
						</span>

						<span class="cl9">
							Fruit
						</span>
					</div>
					-->
					
				</div>
			</div>
		</div>

	</div>
</section>

<!--
<img class="telefon" width="100%"  src="http://www.sumskatajna.rs/images/novo/st-baner-brasno.jpg" alt="IMG-BG" class="rev-slidebg">
<img class="komp" width="100%"  src="http://www.sumskatajna.rs/images/novo/st-brasno-baner-za-telefon.jpg" alt="IMG-BG" class="rev-slidebg">
-->

<!-- Related product
<section class="sec-related bg0 p-b-85">
	<div class="container">
		<div class="wrap-slick9">
			<div class="flex-w flex-sb-m p-b-33 p-rl-15">
				<h3 class="txt-l-112 cl3 m-r-20 respon1 p-tb-15">
					RELATED PRODUCTS
				</h3>

				<div class="wrap-arrow-slick9 flex-w m-t-6"></div>
			</div>

			<div class="slick9">
				
				<div class="item-slick9 p-all-15">
					
					<div class="block1">
						<div class="block1-bg wrap-pic-w bo-all-1 bocl12 hov3 trans-04">
							<img src="{{asset('images/product-02.jpg')}}" alt="IMG">

							<div class="block1-content flex-col-c-m p-b-46">
								<a href="product-single.html" class="txt-m-103 cl3 txt-center hov-cl10 trans-04 js-name-b1">
									Strawberry
								</a>

								<span class="block1-content-more txt-m-104 cl9 p-t-21 trans-04">
									23$
								</span>

								<div class="block1-wrap-icon flex-c-m flex-w trans-05">
									<a href="/proizvod/namaz/1" class="block1-icon flex-c-m wrap-pic-max-w js-addcart-b1">
										<img src="{{asset('images/icons/icon-view.png')}}" alt="ICON">
									</a>

									<a href="#" class="block1-icon flex-c-m wrap-pic-max-w js-addcart-b1">
										<img src="{{asset('images/icons/icon-cart.png')}}" alt="ICON">
									</a>

									<a href="wishlist.html" class="block1-icon flex-c-m wrap-pic-max-w js-addwish-b1">
										<img class="icon-addwish-b1" src="{{asset('images/icons/icon-heart.png')}}" alt="ICON">
										<img class="icon-addedwish-b1" src="{{asset('images/icons/icon-heart2.png')}}" alt="ICON">
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
-->

@stop