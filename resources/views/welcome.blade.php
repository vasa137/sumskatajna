@extends('layout')

@section('meta')
<meta property="og:title" content="ŠUMSKA TAJNA" />
<meta property="og:description" content="ŠUMSKA TAJNA - PREMIUM FOOD " />
<meta property="og:image" content="http://sumskatajna.rs/images/og-slika.jpg" />
@stop


@section('sekcije')
@include('include.popupDialog', ['poruka' => 'Uspešno ste dodali proizvod u korpu. <br/> Da li želite da pregledate korpu?', 'linkUspesno' => '/korpa'])
<!--
<section class="telefon sec-slider" style="width: 100%">
    <div class="rev_slider_wrapper fullwidthbanner-container">
        <div id="rev_slider_6" class="rev_slide fullwidthabanner" data-version="5.4.5" style="display:none">
            <ul>
                
                <li data-transition="fade">
                   
                    <img style="width: 100%" src="/images/namaz-sa-pecurkama-sumska-tajna2.jpg" alt="IMG-BG" class="rev-slidebg">
                    
                   
                    </div>
                                        
               

                 
                    
                </li>
            </ul>               
        </div>
    </div>
</section>
-->

 

<!--
<img class="telefon"  style="width: 100%" src="/images/namaz-sa-pecurkama-sumska-tajna2.jpg" alt="IMG-BG" class="rev-slidebg">
                    
<img class="komp" style="width: 100%" src="/images/namaz-sa-pecurkama-sumska-tajna.jpg" alt="IMG-BG" class="rev-slidebg">

-->

<div style="width: 100%" class="telefon">
    <img width="100%" style="float:left;" src="/images/black11.jpg" alt="IMG-BG" class="rev-slidebg">
    <!--
    <a href="/prodavnica">
    <img width="100%" style="float:left;" src="/images/novo/st-baner-brasno.jpg" alt="IMG-BG" class="rev-slidebg">
    </a>
    -->
    <a href="http://www.sumskatajna.rs/kategorija/Med/8">
    <img width="33.33%" style="float:left;" src="/images/novo/med.jpg" alt="IMG-BG" class="rev-slidebg">
    </a>

    <a href="http://www.sumskatajna.rs/proizvod/Humus-SET/32">
    <img width="33.33%" style="float:left;" src="/images/novo/humus-set.jpg" alt="IMG-BG" class="rev-slidebg">
    </a>

    <a href="http://www.sumskatajna.rs/kategorija/Leblebija/6">
    <img width="33.33%" style="float:left;" src="/images/novo/leblebija.jpg" alt="IMG-BG" class="rev-slidebg">
    </a>
    
</div>



<div  style="width: 100%;" class="komp">
    <!--
    <a href="/prodavnica">
    <img width="100%" style="float:left;" src="/images/novo/st-brasno-baner-za-telefon.jpg" alt="IMG-BG" class="rev-slidebg">
    </a>
    
    <a href="http://www.sumskatajna.rs/kategorija/Med/8">
    <img width="100%" style="float:left;" src="/images/novo/med.jpg" alt="IMG-BG" class="rev-slidebg">
    </a>

    <a href="http://www.sumskatajna.rs/proizvod/Humus-SET/32">
    <img width="100%" style="float:left;" src="/images/novo/humus-set.jpg" alt="IMG-BG" class="rev-slidebg">
    </a>

    <a href="http://www.sumskatajna.rs/kategorija/Leblebija/6">
    <img width="100%" style="float:left;" src="/images/novo/leblebija.jpg" alt="IMG-BG" class="rev-slidebg">
    </a>
    -->
</div>




<!--
<div class="row komp" style=" text-align: center; width: 100%;">
    <div class="container">
        <p><strong>BESPLATNA DOSTAVA ZA SVE PORDUŽBINE PREKO 990 DIN.</strong> <br><br></p>
    </div>                
</div>
-->
<!-- Special Product -->
<section class="sec-specical-product bg0 p-t-80 p-b-80 p-rl-60 p-rl-0-xl">
    <div class="size-a-1 flex-col-c-m p-b-55">
        <img src="/images/icons/stanisic-bio-sumska-tajna.png" alt="IMG">
        <div class="txt-center txt-m-201 cl10 how-pos1-parent m-b-14">
            Izdvajamo iz ponude
        </div>

        <h3 class="txt-center txt-l-101 cl3 respon1">
            Naši proizvodi
        </h3>
    </div>

    <!-- slide5 -->
    <div class="wrap-slick5">
        <div class="slick5">
        @foreach($proizvodi as $proizvod)

                <div class="item-slick5 p-all-15">

                    <div class="block1">
                        <div  class="block1-bg wrap-pic-w bo-all-1 bocl12 hov3 trans-04">
                            @if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike .'.jpg')))
                                <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}">
                                    <img width="100%" src="/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg" alt="<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>">
                                </a>
                            @endif
                            <div   class="txt-center flex-col-c-m p-b-2">
                                <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}" class="txt-m-103 cl3 txt-center hov-cl10 trans-04 js-name-b1">
                                    {{$proizvod->naziv}}<br>
                                </a>

                                @if($proizvod->na_popustu)
                                <strong class="block1-content-more txt-m-112 cl5 p-t-21 trans-04">
                                    {{number_format($proizvod->cena_popust, 0, ',', '.')}} din.
                                </strong>
                                <span>
                                    <del>{{number_format($proizvod->cena, 0, ',', '.')}} din.</del>
                                </span>
                                @else
                                <strong class="block1-content-more txt-m-112 cl5 p-t-21 trans-04">
                                    {{number_format($proizvod->cena, 0, ',', '.')}} din.
                                </strong>
                                @endif

                                <div class="block1-wrap-icon flex-c-m flex-w trans-05">
                                    <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}" class="block1-icon flex-c-m wrap-pic-max-w">
                                        <img src="/images/icons/icon-view.png" alt="ICON">
                                    </a>

                                    <a href="javascript: dodajUKorpu('{!! $proizvod->id !!}', 1);" class="block1-icon flex-c-m wrap-pic-max-w">
                                        <img src="/images/icons/icon-cart.png" alt="ICON">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
        @endforeach
        </div>
        <div class="wrap-dot-slick5 p-rl-15 p-t-40"></div>
        
    </div>

</section>


<div class="size-a-1 flex-col-c-m p-b-55">
        <img src="/images/icons/stanisic-bio-sumska-tajna.png" alt="IMG">

        <h3 class="txt-center txt-l-101 cl3 respon1">
            Naši sastojci
        </h3>
    </div>

<section class="sec-deal">
        <img width="100%" src="{{asset('/images/namaz-sa-pecurkama-sumska-tajna-sastojci.jpg')}}" alt="">
</section>

<!-- Story -->
<section class="sec-story bg0 p-t-150 p-b-80">
    <div class="container">
        <div class="flex-w flex-sb-t">
            <div class="size-w-31 wrap-pic-w how-shadow2 bo-all-15 bocl0 w-full-md">
                <img src="/images/sumska-tajna-namazi-humus-kuvar-avala.jpg" alt="IMG">
            </div>

            <div class="size-w-32 p-t-43 w-full-md">
                <h3 class="txt-center txt-l-401 cl15 p-b-44">
                    Preporuka šefa
                </h3>

                <p class="txt-center txt-m-115 cl6 p-b-25">
                    Svi se slažemo da su pečurke zdrave i bogate različitim vitaminima. 
                </p>

                <p class="txt-center txt-m-115 cl6 p-b-25">
                     U svojim tajnim receptima obožavam da ih koristim. Ali od kako postoje namazi Šumska Tajna, njih konzumiram svakodnevno i sa velikim zadovoljstvom ih koristim u svojim jelima. Obavezno ih probajte.
                </p>



                <div class="flex-w flex-c-b p-t-50 p-t-30">
                    <img class="m-r-55" src="/images/icons/sumska-tajna-namazi-humus-kuvar-avala-potpis.png" alt="SIGN">

                    <div class="flex-col-l p-b-5">
                        <span class="txt-m-401 cl10 p-b-2">
                            Igor Belošević
                        </span>

                        <span class="txt-s-106 cl6">
                            Šef kuhinje i vlasnik restorana AVALA
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>






<div class="sec-testimonials bg12 p-t-120 p-b-80 how-pos3-parent how-pos4-parent">
    <img class="how-pos3 dis-none-xl" src="images/other-04.png" alt="IMG">
    <img class="how-pos4 dis-none-xl" src="images/other-05.png" alt="IMG">

    <div class="container">
        <!-- Slide3 -->
            <div class="wrap-slick3">
                <div class="slick3">
                    
                    <div class="item-slick3">
                        <div class="flex-col-c-m">
                            <div class="layer-slick3 animated visible-false" data-appear="zoomIn" data-delay="100">
                                <div class="wrap-pic-s size-a-3 bo-3-rad-50per bocl10 of-hidden">
                                    <img src="http://sumskatajna.rs/images/osobe/taca.jpg" alt="AVATAR">
                                </div>
                            </div>

                            <div class="layer-slick3 animated visible-false" data-appear="fadeInUp" data-delay="800">
                                <div class="flex-col-c-m p-t-33 p-b-25">
                                    <span class="txt-l-105 cl3 txt-center p-b-9">
                                        Taca Miletic - Mytastypot
                                    </span>
                                </div>
                            </div>

                            <div class="layer-slick3 animated visible-false" data-appear="fadeInUp" data-delay="1600">
                                <p class="txt-center txt-s-104 cl6 size-w-8"> <strong>
                                    Sjajni namazi vrhunskog kvaliteta! Pravi primer domaćeg,  porodičnog biznisa koji osvaja svet! 
                                    </strong>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item-slick3">
                        <div class="flex-col-c-m">
                            <div class="layer-slick3 animated visible-false" data-appear="zoomIn" data-delay="100">
                                <div class="wrap-pic-s size-a-3 bo-3-rad-50per bocl10 of-hidden">
                                    <img src="http://sumskatajna.rs/images/osobe/dejana.jpg" alt="AVATAR">
                                </div>
                            </div>

                            <div class="layer-slick3 animated visible-false" data-appear="fadeInUp" data-delay="800">
                                <div class="flex-col-c-m p-t-33 p-b-25">
                                    <span class="txt-l-105 cl3 txt-center p-b-9">
                                         Dejana Stanković - Mojih 365 dana
                                    </span>
                                </div>
                            </div>

                            <div class="layer-slick3 animated visible-false" data-appear="fadeInUp" data-delay="1600">
                                <p class="txt-center txt-s-104 cl6 size-w-8"> <strong>
                                    Promena načina ishrane, omogućila mi je da spoznam ceo novi svet ukusa i mirisa. Šumsku tajnu sam otkrila sasvim slučajno, mada u slučajnost ne verujem. Sve mi se dopalo, od ambalaže do sastava i ukusa. Od tada, do danas u našem frižideru uvek stoji bar jedna teglica ovih namaza sa leblebijom i pečurkama. Ono što me uvek obraduje jeste uspeh domaćih proizvoda, posebno kad je očigledno sa koliko ljubavi i truda su napravljeni.
                                </strong>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="item-slick3">
                        <div class="flex-col-c-m">
                            <div class="layer-slick3 animated visible-false" data-appear="zoomIn" data-delay="100">
                                <div class="wrap-pic-s size-a-3 bo-3-rad-50per bocl10 of-hidden">
                                    <img src="http://sumskatajna.rs/images/osobe/minjina-kuhinjica.jpg" alt="AVATAR">
                                </div>
                            </div>

                            <div class="layer-slick3 animated visible-false" data-appear="fadeInUp" data-delay="800">
                                <div class="flex-col-c-m p-t-33 p-b-25">
                                    <span class="txt-l-105 cl3 txt-center p-b-9">
                                         Miroslava Bogdanovski - Minjina Kuhinjica
                                    </span>
                                </div>
                            </div>

                            <div class="layer-slick3 animated visible-false" data-appear="fadeInUp" data-delay="1600">
                                <p class="txt-center txt-s-104 cl6 size-w-8"> <strong>
                                    Kada poželim laganiji obrok, često posegnem za finim namazima iz Šumske tajne i tostiranim hlebom. Moj omiljeni namaz je sa crnim tartufom, često ga namažem na tek ispečeni hleb i to je pravo uživanje za sva čula :)
                                    <br>
                                    Namazi su odlični jer možete da ih kombinujete, a i da napunite slane korpice i poslužite na slavskoj trpezi.
                                </strong>
                                </p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="item-slick3">
                        <div class="flex-col-c-m">
                            <div class="layer-slick3 animated visible-false" data-appear="zoomIn" data-delay="100">
                                <div class="wrap-pic-s size-a-3 bo-3-rad-50per bocl10 of-hidden">
                                    <img src="http://sumskatajna.rs/images/osobe/tanja.jpg" alt="AVATAR">
                                </div>
                            </div>

                            <div class="layer-slick3 animated visible-false" data-appear="fadeInUp" data-delay="800">
                                <div class="flex-col-c-m p-t-33 p-b-25">
                                    <span class="txt-l-105 cl3 txt-center p-b-9">
                                         Tanja Oladžija - Zdravo Tanja
                                
                                    </span>
                                </div>
                            </div>

                            <div class="layer-slick3 animated visible-false" data-appear="fadeInUp" data-delay="1600">
                                <p class="txt-center txt-s-104 cl6 size-w-8"> <strong>
                                     Volim Šumsku Tajnu zbog sjajnog ukusa i odličnog sastava, koji mi je kao nutricionisti uvek važna stavka. Iz tog razloga Šumska Tajna se često nađe i u mojim planovima ishrane i svi moji klijenti uvek joj se raduju. Lično mi je omiljeni tartuf, mada je teško izdvojiti samo jedan ukus.</strong>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item-slick3">
                        <div class="flex-col-c-m">
                            <div class="layer-slick3 animated visible-false" data-appear="zoomIn" data-delay="100">
                                <div class="wrap-pic-s size-a-3 bo-3-rad-50per bocl10 of-hidden">
                                    <img src="http://sumskatajna.rs/images/osobe/mamin-zivot.jpg" alt="AVATAR">
                                </div>
                            </div>

                            <div class="layer-slick3 animated visible-false" data-appear="fadeInUp" data-delay="800">
                                <div class="flex-col-c-m p-t-33 p-b-25">
                                    <span class="txt-l-105 cl3 txt-center p-b-9">
                                         Danijela Stojanović - Mamin život
                                
                                    </span>
                                </div>
                            </div>

                            <div class="layer-slick3 animated visible-false" data-appear="fadeInUp" data-delay="1600">
                                <p class="txt-center txt-s-104 cl6 size-w-8"> <strong>
                                      Kao neko ko mnogo voli leblebiju i pečurke, svih vrsta, bila sam oduševljena kada sam čula da namaz od istih postoji, Šumska Tajna.<br>
                                Bila je to ljubav na prvi zalogaj! Oduševljena sam bila postojanim i jako lepim ukusom, koji je osvojio i moju tada jednoipogodišnju ćerku Saru.</strong>
                                
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="wrap-slick3-dots p-t-50"></div>
            </div>
    </div>
</div>

    


<section class="sec-blog bg12 p-t-80 p-b-52">
    <div class="container">
        <div class="size-a-1 flex-col-c-m p-b-70">
            <img src="/images/icons/stanisic-bio-sumska-tajna.png" alt="IMG">
            <div class="txt-center txt-m-201 cl10 how-pos1-parent m-b-14">
                Šumska tajna se savršeno slaže uz sva jela

                <div class="how-pos1">
                    
                </div>
            </div>

            <h3 class="txt-center txt-l-101 cl3 respon1">
                RECEPTI
            </h3>
        </div>

        <div class="row justify-content-center">
            <div class="col-sm-9 col-md-8 col-lg-4 m-rl-auto-md">
                <div class="p-b-63">
                    <a href="/recept/{{$recepti[count($recepti)-1]->id}}" class="wrap-pic-w hov4">
                        <img src="http://sumskatajna.rs/images/clanci/{{$recepti[count($recepti)-1]->id}}/{{$recepti[count($recepti)-1]->naslov}}.jpg" alt="BLOG">
                    </a>

                    <div class="size-w-43 how-pos7-parent m-rl-auto js-height">
                        <div class="flex-col-c w-full how-pos7 bg0 how-shadow1 p-rl-35 p-b-35 p-t-28 p-rl-15-ssm js-height-child">
                            <a  href="/recept/{{$recepti[count($recepti)-1]->id}}" class="txt-center txt-m-109 cl3 hov-cl10 trans-04 p-b-6">
                                {{$recepti[count($recepti)-1]->naslov}}
                            </a>                        

                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 col-md-8 col-lg-4 m-rl-auto-md">
                <div class="p-b-63">
                    <a href="/recept/{{$recepti[count($recepti)-2]->id}}" class="wrap-pic-w hov4">
                        <img src="http://sumskatajna.rs/images/clanci/{{$recepti[count($recepti)-2]->id}}/{{$recepti[count($recepti)-2]->naslov}}.jpg" alt="BLOG">
                    </a>

                    <div class="size-w-43 how-pos7-parent m-rl-auto js-height">
                        <div class="flex-col-c w-full how-pos7 bg0 how-shadow1 p-rl-35 p-b-35 p-t-28 p-rl-15-ssm js-height-child">
                            <a  href="/recept/{{$recepti[count($recepti)-2]->id}}" class="txt-center txt-m-109 cl3 hov-cl10 trans-04 p-b-6">
                                {{$recepti[count($recepti)-2]->naslov}}
                            </a>                        

                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 col-md-8 col-lg-4 m-rl-auto-md">
                <div class="p-b-63">
                    <a href="/recept/{{$recepti[count($recepti)-3]->id}}" class="wrap-pic-w hov4">
                        <img src="http://sumskatajna.rs/images/clanci/{{$recepti[count($recepti)-3]->id}}/{{$recepti[count($recepti)-3]->naslov}}.jpg" alt="BLOG">
                    </a>

                    <div class="size-w-43 how-pos7-parent m-rl-auto js-height">
                        <div class="flex-col-c w-full how-pos7 bg0 how-shadow1 p-rl-35 p-b-35 p-t-28 p-rl-15-ssm js-height-child">
                            <a  href="/recept/{{$recepti[count($recepti)-3]->id}}" class="txt-center txt-m-109 cl3 hov-cl10 trans-04 p-b-6">
                                {{$recepti[count($recepti)-3]->naslov}}
                            </a>                        

                        
                        </div>
                    </div>
                </div>
            </div>

            
        </div>
    </div>
</section>



<section class="sec-blog bg0 p-t-80 p-b-50">
    <div class="container">
        <div class="size-a-1 flex-col-c-m p-b-70">
            <img src="/images/icons/stanisic-bio-sumska-tajna.png" alt="IMG">
            <div class="txt-center txt-m-201 cl10 how-pos1-parent m-b-14">
                Šumska tajna - Blog

                <div class="how-pos1">
                    
                </div>
            </div>

            <h3 class="txt-center txt-l-101 cl3 respon1">
                Blog
            </h3>
        </div>

        <div class="row">
            <div class="col-sm-10 col-md-8 col-lg-6 p-b-30 m-rl-auto">
                <div class="p-r-30 p-r-0-lg">
                    <div class="flex-w flex-sb-t">
                        <a href="/blog/{{$blog[count($blog)-1]->id}}" class="wrap-pic-w hov4 w-full pos-relative w-full-sm m-r-0-sm m-b-15">
                            <img src="http://sumskatajna.rs/images/clanci/{{$blog[count($blog)-1]->id}}/{{$blog[count($blog)-1]->id}}.jpg" alt="BLOG">

                            
                        </a>

                        <div class="w-full p-t-12 w-full-sm m-b-15">
                            <h4 class="p-b-10">
                                <a href="/blog/{{$blog[count($blog)-1]->id}}" class="txt-m-120 cl3 hov-cl10 trans-04">
                                    {{$blog[count($blog)-1]->naslov}}
                                </a>
                            </h4>

                            <p class="txt-s-101 cl6 p-b-20">
                                {{$blog[count($blog)-1]->uvod}}
                            </p>

                            <div class="how-line2 p-l-40">
                                <a href="/blog/{{$blog[count($blog)-1]->id}}" class="txt-s-105 cl9 hov-cl10 trans-04">
                                    Detaljnije
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-10 col-md-8 col-lg-6 p-b-30 m-rl-auto">
                <div class="row">
                    <div class="col-12 m-rl-auto p-b-15">
                        <div class="flex-w flex-sb-t">
                            <a href="/blog/{{$blog[count($blog)-2]->id}}" class="wrap-pic-w hov4 size-w-29 pos-relative w-full-sm m-r-0-sm m-b-15">
                                <img src="http://sumskatajna.rs/images/clanci/{{$blog[count($blog)-2]->id}}/{{$blog[count($blog)-2]->id}}.jpg" alt="BLOG">
                            </a>

                            <div class="size-w-30 p-t-12 w-full-sm m-b-15">
                                <h4 class="p-b-10">
                                    <a href="/blog/{{$blog[count($blog)-2]->id}}" class="txt-m-109 cl3 hov-cl10 trans-04">
                                        {{$blog[count($blog)-2]->naslov}}
                                    </a>
                                </h4>

                                <p class="txt-s-101 cl6 p-b-20">
                                    {{$blog[count($blog)-2]->uvod}}
                                </p>

                                <div class="how-line2 p-l-40">
                                    <a href="/blog/{{$blog[count($blog)-2]->id}}" class="txt-s-105 cl9 hov-cl10 trans-04">
                                        Detaljnije
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 m-rl-auto p-b-15">
                        <div class="flex-w flex-sb-t">
                            <a href="/blog/{{$blog[count($blog)-3]->id}}" class="wrap-pic-w hov4 size-w-29 pos-relative w-full-sm m-r-0-sm m-b-15">
                                <img src="http://sumskatajna.rs/images/clanci/{{$blog[count($blog)-3]->id}}/{{$blog[count($blog)-3]->id}}.jpg" alt="BLOG">
                            </a>

                            <div class="size-w-30 p-t-12 w-full-sm m-b-15">
                                <h4 class="p-b-10">
                                    <a href="/blog/{{$blog[count($blog)-3]->id}}" class="txt-m-109 cl3 hov-cl10 trans-04">
                                        {{$blog[count($blog)-3]->naslov}}
                                    </a>
                                </h4>

                                <p class="txt-s-101 cl6 p-b-20">
                                    {{$blog[count($blog)-3]->uvod}}
                                </p>

                                <div class="how-line2 p-l-40">
                                    <a href="/blog/{{$blog[count($blog)-3]->id}}" class="txt-s-105 cl9 hov-cl10 trans-04">
                                        Detaljnije
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Logo -->
<div class="sec-logo bg0">
    <div class="container">
        <div class="flex-w flex-sa-m bo-t-1 bocl13 p-tb-20">

            <a href="#" class="dis-block how2 p-rl-15 m-tb-20">
                <img  height="45" src="{{asset('/images/prirodni-vegan-namazi-ikonice/idea.png')}}">
            </a>
            <a href="#" class="dis-block how2 p-rl-15 m-tb-20">
                <img  height="45" src="{{asset('/images/prirodni-vegan-namazi-ikonice/roda.png')}}">
            </a>
            <a href="#" class="dis-block how2 p-rl-15 m-tb-20">
                <img  height="45" src="{{asset('/images/prirodni-vegan-namazi-ikonice/aroma.png')}}">
            </a>
            <a href="#" class="dis-block how2 p-rl-15 m-tb-20">
                <img height="45" src="{{asset('/images/prirodni-vegan-namazi-ikonice/univer.png')}}">
            </a>
            <a href="#" class="dis-block how2 p-rl-15 m-tb-20">
                <img height="45" src="{{asset('/images/prirodni-vegan-namazi-ikonice/merkator.png')}}">
            </a>
        </div>
    </div>
</div>

<!-- Subscribe 
<section class="sec-subscribe bg13 p-t-155 p-b-65" style="height: 429px; background-image: url(/images/namaz-sa-pecurkama-sumska-tajna-footer.jpg);">>
    <div class="container">
        <div class="row">
            <div class="col-md-5 p-tb-15">
                <div class="h-full flex-col-m">
                    <h4 class="txt-m-110 cl3 p-b-4">
                        Subscribe Newsletter.
                    </h4>

                    <p class="txt-s-101 cl6">
                        Get e-mail updates about our latest shop and special offers.
                    </p>
                </div>
            </div>

            <div class="col-md-7 p-tb-15">
                <form class="flex-w flex-m h-full">
                    <input class="size-a-6 txt-s-106 cl6 plh0 p-rl-30 w-full-sm" type="text" name="email" placeholder="Your email address">
                    <button class="bg10 size-a-5 txt-s-107 cl0 p-rl-15 trans-04 hov-btn2 mt-4 mt-sm-0">
                        Subscribe
                    </button>
                </form>
            </div>
        </div>
    </div>
</section>
-->
@stop