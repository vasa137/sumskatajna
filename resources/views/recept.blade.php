@extends('layout')

@section('title')
Recepti - {{$clanak->naslov}}
@stop

@section('meta')
<meta property="og:url"                content="http://sumskatajna.rs/recept/{{$clanak->id}}" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="{{$clanak->naslov}}" />
<meta property="og:description"        content="{{$clanak->uvod}}" />
<meta property="og:image"              content="http://sumskatajna.rs/images/clanci/{{$clanak->slika}}" />

<style>

a {
  font-weight: bold;
}

</style>
@stop


@section('sekcije')
<!-- Title page -->
<section class="bg-img1" style="background-image: url(http://sumskatajna.rs/images/pozadina-heder-namazi-sa-pecurkama.jpg);">
	<div class="container">
		<div class="txt-center p-t-160 p-b-165">
			<h2 class="txt-l-101 cl0 txt-center p-b-14 respon1">
				{{$clanak->naslov}}
			</h2>

			<span class="txt-m-201 cl0 flex-c-m flex-w">
				<a href="/" class="txt-m-201 cl0 hov-cl10 trans-04 m-r-6">
					Naslovna
				</a>
				<a href="/blog" class="txt-m-201 cl0 hov-cl10 trans-04 m-r-6">
					/ Recepti
				</a>
				<span>
					/ {{$clanak->naslov}}
				</span>
			</span>
		</div>
	</div>
</section>

<!-- Content page -->
<section class="bg0 p-t-100 p-b-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 m-rl-auto p-b-80">
				<!-- detail blog -->
				<div class="p-b-50">
					<div class="wrap-pic-w">
						<img src="http://sumskatajna.rs/images/clanci/{{$clanak->id}}/{{$clanak->naslov}}.jpg" alt="BLOG">
					</div>

					<div class="p-t-30">
						<h4 class="txt-m-125 cl3">
							{{$clanak->naslov}}
						</h4>

						<p class="txt-s-101 cl6 p-b-12">
							{!!$clanak->tekst!!}
						</p>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>		
@stop