<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ne postoji korisnički nalog sa unetim kredencijalima.',
    'throttle' => 'Previše puta ste pokušali. Možete pokušati ponovo za :seconds sekundi.',
    'blocked' => 'Korisnički nalog je blokiran.'
];
